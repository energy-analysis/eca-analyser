-- @component SoundSystem

systemOn = 0

function function dyn_res_cons()
  return 2*systemOn
end

-- on
function on_time(x)
  return 0
end

function on_resource(x)
  return 0
end

function on_definition(x)
  systemOn = 1
  return 1
end

-- off
function off_time(x)
  return 0
end

function off_resource(x)
  return 0
end

function off_definition()
  systemOn = 0
  return 0
end

-- playbeep
function playBeep_time(x)
  return 5
end

function playBeep_resource(x)
  return 15
end

function playBeep_definition(x)
  if systemOn then
    x = 1 
  else
    x = 0
  end
  return x
end

-- sleep
function sleep_time(x)
  return 50
end

function sleep_resource(x)
  return 0
end

function sleep_definition(x)
  return 0
end