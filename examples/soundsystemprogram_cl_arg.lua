--[[
	Example of usage of command line arguments
	Use the command line argument
		./eca2 soundsystemprogram_cl_arg.lua soundsystem1.lua max=<n>
	to do n iterations. For Lua, this is also possible in combination with simulation:
		./eca2 -simulate soundsystemprogram_cl_arg.lua soundsystem1.lua max=<n>
	Use
		./eca2 soundsystemprogram_cl_arg.eca soundsystem1.lua max=<n>
	for the eca variant
]]

SoundSystem_on(0)
i = 0
while i < max do
	SoundSystem_playBeep(0)
	SoundSystem_sleep(0)
   i = i + 1
end
SoundSystem_off(0)