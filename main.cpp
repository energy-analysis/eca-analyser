#include <iostream>
#include <stdio.h>
#include <vector>
#include <fstream>
#include "libtypesystem.h"
#include "LuaComponent.h"
#include "LuaParser.h"
#include "LuaSimulator.h"
#include "parse.h"
#include "ReadComponent.h"
#include "ValueInt.h"
#include "ValueString.h"
#include "Value.h"

using namespace std;
using namespace bitpowder::lib;

struct CStringSpanner {
    mutable bool previousCharWasEscapeChar = false;
    mutable bool didSeeEscapeChar = false;
            void reset() const {
                previousCharWasEscapeChar = false;
                didSeeEscapeChar = false;
            }
    bool operator()(char in) const {
        bool retval = previousCharWasEscapeChar || in != '"';
        previousCharWasEscapeChar = !previousCharWasEscapeChar && in == '\\';
        didSeeEscapeChar = didSeeEscapeChar || previousCharWasEscapeChar;
        return retval; // true = accept this char
    }
};

LuaComponent* readLuaComponent(ifstream &compFile, String &compFileName){
	string lua_code((istreambuf_iterator<char>(compFile)), istreambuf_iterator<char>());

	// Read component name from the comment on the first line
	string firstLine;
	getline(istringstream(lua_code), firstLine);
	String rem(firstLine);
	bool error = false;
	while(StringParser(rem).accept(" ", "\t").remainder(rem));
	if(!StringParser(rem).accept("--").remainder(rem))
		error = true;
	while(StringParser(rem).accept(" ", "\t").remainder(rem));
	if(!StringParser(rem).accept("@component").remainder(rem))
		error = true;
	while(StringParser(rem).accept(" ", "\t").remainder(rem));
	String compName;
	if(!StringParser(rem).span(Alpha() + C('_') + Num()).matched(compName))
		error = true;

	compFile.close();

	if(error){
		throw runtime_error("Error: first line of component file " + compFileName.stl() + " should start with comment tag with component name, eg:\n-- @component SoundSystem");
	}

	return new LuaComponent(compName.stl(), compFileName.stl(), LuaComponent::Mode::file);
}



int main(int argc, char** argv)
{
    try{
        if (argc < 2) {
            cerr << "Usage: " << argv[0] << "[-simulate] <program_filename> (<component_filenames>)* (<var_name>=<init_value>)*" << endl;
            exit(EXIT_FAILURE);
        }

        // == Init memory
        MemoryPool mp;

        map<string, Component*> components;
        FunctionEnvironment env = FunctionEnvironment(1, 2, 3, 4, 5, 6, 7, vector<Function*>());
        State programState = State();

        Rule* root = nullptr;
        LuaSimulator* sim = nullptr;
        int argi = 1;
        bool simulateOption = false;

        // == Read options
        if(strncmp(argv[argi], "-simulate", 20) == 0){
            simulateOption = true;
            argi++;
        }

        // == Read the program file
        String progFileName(argv[argi]);
        cout << "Reading " << progFileName << endl;

        if(simulateOption)
        {
            if(!progFileName.endsWith(".lua"))
                cout << "Warning: file name '" << progFileName << "' does not end with '.lua'. Only simulation of Lua is possible. Interpreting code as Lua." << endl;
            string fileNameString = progFileName.stl();
            sim = LuaSimulator::createSimulator(fileNameString, LuaSimulator::Mode::file);
        } else
        {
            ASTParseResult result;
            if(progFileName.endsWith(".eca")){
                result = ParseECAFile(argv[argi], mp);
                result.result()->deriveAssignmentSorts();
            }
            else if(progFileName.endsWith(".lua"))
                result = ParseLuaFile(argv[argi], mp);
            else
            {
                std::cerr << "Extension should be one of {.eca, .lua}" << std::endl;
                exit(EXIT_FAILURE);
            }
            if (!result)
            {
                cout << "Error during parsing of " << progFileName << endl;
                exit(EXIT_FAILURE);
            }
            root = result.result()->getTypeRule();
        }

        argi++;


        // == Read the component files and initial state
        for(int i = argi; i < argc; i++)
        {
            String cl_arg = argv[i];
            if(cl_arg.contains("="))
            {
                String value;
                String var_name = cl_arg.split("=", value);
                CStringSpanner spanner;
                String match;
                int n;
                if(StringParser(value).accept('"').span(spanner).matched(match).accept('"').empty()) {
                    match = match.unescape(String::Escape::LikeC, mp);
                    if(simulateOption)
                        sim->declareVar(var_name.stl(), new ValueString(match.stl()));
                    else
                        programState.declareLocalVar(var_name.stl(), new ValueString(match.stl()));
                }
                else if(StringParser(value).number(n).empty()){
                    if(simulateOption)
                        sim->declareVar(var_name.stl(), new ValueInt(n));
                    else
                        programState.declareLocalVar(var_name.stl(), new ValueInt(n));
                } else {
                    cerr << "Error in command line argument " << cl_arg << endl;
                    exit(EXIT_FAILURE);
                }
            }
            else {
                if(!(cl_arg.endsWith(".eca") || cl_arg.endsWith(".lua")))
                {
                    cerr << cl_arg << ": Extension should be one of {.eca, .lua}" << endl;
                    exit(EXIT_FAILURE);
                }
                ifstream compFile;
                compFile.open(argv[i]);
                if(!compFile)
                {
                    cerr << "Error while opening " << argv[i] << endl;
                    return -1;
                }
                cout << "Reading " << argv[i] << endl;
                if(cl_arg.endsWith(".eca"))
                {
                    ECAComponent* comp = readECAComponent(compFile);
                    components.insert(make_pair(comp->getName(), comp));
                }
                else { //filename extension is .lua
                    LuaComponent* comp = readLuaComponent(compFile, cl_arg);
                    components.insert(make_pair(comp->getName(), comp));
                }
                compFile.close();
            }
        }

        if(simulateOption){
            sim->registerComponents(&components);
            SimulationResult s = sim->run();
            cout << "Simulated resource usage: " << s.resource << endl;
            cout << "Simulated time: " << s.time << " ns" << endl;
        } else {
            EvaluationResult s = root->evaluateRule(programState, env, components);
            cout << "Modeled resource usage: " << s.getResourceAmount() << endl;
            cout << "Modeled time: " << s.getTime() << " ns" << endl;
        }

		return 0;
	}
	catch (const runtime_error& e)
    {
        cerr << e.what() << endl;
        exit(EXIT_FAILURE);
    }

}
