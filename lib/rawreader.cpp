/**
Copyright 2014-2018 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rawreader.h"
#include "raw-common.h"

using namespace bitpowder;
using namespace lib;

static_assert(-1 == ~0, "two complements machine required for correct writeInt (signed) behaviour");

const char* const RawReader::INVALID = OLD_STYLE_CAST((const char*) - 1);

double RawReader::readPlatformIndependantDouble() {
	int64_t significand = readInt64();
	int16_t exp = readInt16();
	if (!valid())
		return 0;
	if (significand == 0 && exp == std::numeric_limits<int16_t>::max())
		return 0;
	if (significand == 0 && exp == std::numeric_limits<int16_t>::max() - 1)
		return std::numeric_limits<double>::infinity();
	if (significand == 0 && exp == std::numeric_limits<int16_t>::max() - 2)
		return std::numeric_limits<double>::quiet_NaN();
	double significandFloat = double(significand) / std::numeric_limits<int64_t>::max();
	significandFloat /= 2.0;
	if (significand < 0)
		significandFloat -= nextafter(0.5, 0);
	else
		significandFloat += 0.5;
	double retval = ldexp(significandFloat, exp);
	return retval;
}

float RawReader::readFloat() {
	uint32_t source = readUnsignedInt32();
	float retval;
	static_assert(sizeof(retval) == sizeof(source), "readFloat depend on size of 32-bit int is equal to size of float");
	static_assert(std::numeric_limits<float>::is_iec559, "hardware format of float used shoud be IEEE 754");
	memcpy(&retval, &source, sizeof(retval));
	return retval;
}

double RawReader::readDouble() {
	uint64_t source = readUnsignedInt64();
	double retval;
	static_assert(sizeof(retval) == sizeof(source), "readFloat depend on size of 64-bit int is equal to size of double");
	static_assert(std::numeric_limits<double>::is_iec559, "hardware format of double used shoud be IEEE 754");
	memcpy(&retval, &source, sizeof(retval));
	return retval;
}


String RawWriter::remainingBytes() {
	return valid() ? String{ptr, len} :
				 String{};
}

RawWriter RawWriter::writerBytes(size_t bytes) {
	if (len < bytes) {
		isValid = false;
		return {};
	}
	RawWriter retval = {ptr, bytes};
	substring(bytes);
	return retval;
}

void RawWriter::skipBytes(size_t bytes) {
	if (len < bytes) {
		isValid = false;
		return;
	}
	substring(bytes);
}

void RawWriter::writeByte(char byte) {
	if (!isValid || len < 1) {
		isValid = false;
		return;
	}
	*ptr = byte;
	substring(1);
}

void RawWriter::writeUnsignedInt16(uint16_t u16) {
	if (!isValid || len < 2) {
		isValid = false;
		return;
	}
	u16 = hton(u16);
	memcpy(ptr, &u16, 2);
	substring(2);
}

void RawWriter::writeInt16(int16_t s16) {
	writeUnsignedInt16(static_cast<uint16_t>(s16));
}

void RawWriter::writeUnsignedInt32(uint32_t u32) {
	if (!isValid || len < 4) {
		isValid = false;
		return;
	}
	u32 = hton(u32);
	memcpy(ptr, &u32, 4);
	substring(4);
}

void RawWriter::writeInt32(int32_t s32) {
	writeUnsignedInt32(static_cast<uint32_t>(s32));
}

void RawWriter::writeUnsignedInt64(uint64_t u64) {
	if (!isValid || len < 8) {
		isValid = false;
		return;
	}
	u64 = hton(u64);
	memcpy(ptr, &u64, 8);
	substring(8);
}

void RawWriter::writeInt64(int64_t s64) {
	writeUnsignedInt64(static_cast<uint64_t>(s64));
}

unsigned char RawWriter::SizeOfUnsignedIntVar(uint64_t u64) {
	if (u64 <= 0x7F)
		return 1;
	if (u64 <= 0x3FFF)
		return 2;
	if (u64 <= 0x1FFFFF)
		return 3;
	if (u64 <= 0x0FFFFFFF)
		return 4;
	if (u64 <= 0x07FFFFFFFF)
		return 5;
	if (u64 <= 0x03FFFFFFFFFFLL)
		return 6;
	if (u64 <= 0x01FFFFFFFFFFFFLL)
		return 7;
	if (u64 <= 0x00FFFFFFFFFFFFFFLL)
		return 8;
	return 9;
}

unsigned char RawWriter::SizeOfUnsigned(uint64_t u64) {
	if (u64 <= 0xFF)
		return 1;
	if (u64 <= 0xFFFF)
		return 2;
	if (u64 <= 0xFFFFFF)
		return 3;
	if (u64 <= 0xFFFFFFFF)
		return 4;
	if (u64 <= 0xFFFFFFFFFF)
		return 5;
	if (u64 <= 0xFFFFFFFFFFFFLL)
		return 6;
	if (u64 <= 0xFFFFFFFFFFFFFFLL)
		return 7;
	return 8;
}

void RawWriter::writeUnsignedIntVar(uint64_t u64) {
	if (u64 <= 0x7F) { // 1 bytes
		writeUnsignedByte(uint8_t(u64));
		return;
	}
	if (u64 <= 0x3FFF) { // 2 bytes
		writeUnsignedInt16(uint16_t(u64 | (0b10000000U << 8)));
		return;
	}
	if (u64 <= 0x1FFFFF) { // 3 bytes
		writeUnsignedByte(uint8_t(u64 >> 16 | 0b11000000U));
		writeUnsignedInt16(uint16_t(u64));
		return;
	}
	if (u64 <= 0x0FFFFFFF) { // 4 bytes
		writeUnsignedInt32(uint32_t(u64 | (0b11100000U << 24)));
		return;
	}
	if (u64 <= 0x07FFFFFFFF) { // 5 bytes
		writeUnsignedByte(uint8_t(u64 >> 32 | 0b11110000U));
		writeUnsignedInt32(uint32_t(u64));
		return;
	}
	if (u64 <= 0x03FFFFFFFFFFLL) { // 6 bytes
		writeUnsignedInt16(uint16_t(u64 >> 32 | (0b11111000U << 8)));
		writeUnsignedInt32(uint32_t(u64));
		return;
	}
	if (u64 <= 0x01FFFFFFFFFFFFLL) { // 7 bytes
		writeUnsignedByte(uint8_t(u64 >> 48 | 0b11111100U));
		writeUnsignedInt16(uint16_t(u64 >> 32));
		writeUnsignedInt32(uint32_t(u64));
		return;
	}
	if (u64 <= 0x00FFFFFFFFFFFFFFLL) { // 8 bytes
		writeUnsignedInt64(u64 | (0b11111110ULL << 56));
		return;
	}
	writeUnsignedByte(0xFFU);
	writeUnsignedInt64(u64);
}

unsigned char RawWriter::SizeOfIntVar(int64_t i64) {
	// equivalent:
	//int leadingSignBits = __builtin_clrsbll(i64);
	//return 10-(leadingSignBits/7);
	if (-0x40 <= i64 && i64 <= 0x3F)
		return 1;
	if (-0x2000 <= i64 && i64 <= 0x1FFF)
		return 2;
	if (-0x100000 <= i64 && i64 <= 0x0FFFFF)
		return 3;
	if (-0x08000000 <= i64 && i64 <= 0x07FFFFFF)
		return 4;
	if (-0x0400000000 <= i64 && i64 <= 0x03FFFFFFFF)
		return 5;
	if (-0x020000000000LL <= i64 && i64 <= 0x01FFFFFFFFFFLL)
		return 6;
	if (-0x01000000000000LL <= i64 && i64 <= 0x00FFFFFFFFFFFFLL)
		return 7;
	if (-0x0080000000000000LL <= i64 && i64 <= 0x007FFFFFFFFFFFFFLL)
		return 8;
	return 9;
}

unsigned char RawWriter::SizeOfSigned(int64_t i64) {
	if (-0x80 <= i64 && i64 <= 0x7F)
		return 1;
	if (-0x8000 <= i64 && i64 <= 0x7FFF)
		return 2;
	if (-0x800000 <= i64 && i64 <= 0x7FFFFF)
		return 3;
	if (-0x80000000 <= i64 && i64 <= 0x7FFFFFFF)
		return 4;
	if (-0x8000000000LL <= i64 && i64 <= 0x7FFFFFFFFFLL)
		return 5;
	if (-0x800000000000LL <= i64 && i64 <= 0x7FFFFFFFFFFFLL)
		return 6;
	if (-0x800000000000LL <= i64 && i64 <= 0x7FFFFFFFFFFFLL)
		return 7;
	return 8;
}

void RawWriter::writeSigned(int64_t i64, unsigned char length) {
	if (i64 < 0) {
		// keep the correct sign
		if (length == 1 && -0x80 > i64)
			i64 = -0x80;
		else if (length == 2 && -0x8000 > i64)
			i64 = -0x8000;
		else if (length == 3 && -0x800000 > i64)
			i64 = -0x800000;
		else if (length == 4 && -0x80000000 > i64)
			i64 = -0x800000;
		else if (length == 5 && -0x8000000000LL > i64)
			i64 = -0x80000000LL;
		else if (length == 6 && -0x800000000000LL > i64)
			i64 = -0x8000000000LL;
		else if (length == 7 && -0x800000000000LL > i64)
			i64 = -0x800000000000LL;
	}
	writeUnsigned(*reinterpret_cast<uint64_t*>(&i64), length);
}

void RawWriter::writeUnsigned(uint64_t u64, unsigned char length) {
	if (length == 1) {
		writeUnsignedByte(uint8_t(u64));
		return;
	}
	if (length == 2) {
		writeUnsignedInt16(uint16_t(u64));
		return;
	}
	if (length == 3) {
		writeUnsignedByte(uint8_t(u64 >> 16));
		writeUnsignedInt16(uint16_t(u64));
		return;
	}
	if (length == 4) {
		writeUnsignedInt32(uint32_t(u64));
		return;
	}
	if (length == 5) {
		writeUnsignedByte(uint8_t(u64 >> 32));
		writeUnsignedInt32(uint32_t(u64));
		return;
	}
	if (length == 6) {
		writeUnsignedInt16(uint16_t(u64 >> 32));
		writeUnsignedInt32(uint32_t(u64));
		return;
	}
	if (length == 7) {
		writeUnsignedByte(uint8_t(u64 >> 48));
		writeUnsignedInt16(uint16_t(u64 >> 32));
		writeUnsignedInt32(uint32_t(u64));
		return;
	}
	writeUnsignedInt64(u64);
}

void RawWriter::writeIntVar(int64_t i64) {
	if (-0x40 <= i64 && i64 <= 0x3F) { // 1 byte
		writeUnsignedByte(i64 & 0x7FU);
		return;
	}
	if (-0x2000 <= i64 && i64 <= 0x1FFF) { // 2 bytes
		writeUnsignedInt16((i64 & 0x3FFFU) | (0b10000000U << 8));
		return;
	}
	if (-0x100000 <= i64 && i64 <= 0x0FFFFF) { // 3 bytes
		writeUnsignedByte(((i64 >> 16) & 0b00011111U) | 0b11000000U);
		writeUnsignedInt16(static_cast<uint16_t>(i64));
		return;
	}
	if (-0x08000000 <= i64 && i64 <= 0x07FFFFFF) { // 4 bytes
		writeUnsignedInt32(static_cast<uint32_t>(i64 & 0x0FFFFFFFU) | (0b11100000U << 24));
		return;
	}
	if (-0x0400000000 <= i64 && i64 <= 0x03FFFFFFFF) { // 5 bytes
		writeUnsignedByte(((i64 >> 32) & 0b00000111U) | 0b11110000U);
		writeUnsignedInt32(static_cast<uint32_t>(i64));
		return;
	}
	if (-0x020000000000LL <= i64 && i64 <= 0x01FFFFFFFFFFLL) { // 6 bytes
		writeUnsignedInt16(static_cast<uint16_t>(((i64 >> 32) & 0x03FFU) | (0b11111000U << 8)));
		writeUnsignedInt32(static_cast<uint32_t>(i64));
		return;
	}
	if (-0x01000000000000LL <= i64 && i64 <= 0x00FFFFFFFFFFFFLL) { // 7 bytes
		writeUnsignedByte(((i64 >> 48) & 0b00000001U) | 0b11111100U);
		writeUnsignedInt16(static_cast<uint16_t>(i64 >> 32));
		writeUnsignedInt32(static_cast<uint32_t>(i64));
		return;
	}
	if (-0x0080000000000000LL <= i64 && i64 <= 0x007FFFFFFFFFFFFFLL) { // 8 bytes
		writeUnsignedInt64((static_cast<uint64_t>(i64) & 0x00FFFFFFFFFFFFFFULL) | (0b11111110ULL << 56));
		return;
	}
	writeUnsignedByte(0xFF);
	writeInt64(i64);
}

void RawWriter::writePlatformIndependantDouble(double d) {
	if (std::isinf(d)) {
		writeInt64(0);
		writeInt16(std::numeric_limits<int16_t>::max() - 1);
		return;
	}
	if (std::isnan(d)) {
		writeInt64(0);
		writeInt16(std::numeric_limits<int16_t>::max() - 2);
		return;
	}
	int exp = 0;
	double significand = frexp(d, &exp);
	if (-0.4 < significand && significand < 0.4 && exp == 0) {
		// special case: zero
		writeInt64(0);
		writeInt16(std::numeric_limits<int16_t>::max()); // can not be zero, to avoid colloision with representation of 0.5
	} else {
		// significand is [0.5..1.0] (0.5 included, 1.0 excluded)
		// significand can also be negative!
		bool negative = significand < 0;
		if (negative) {
			// d is negative
			significand += nextafter(0.5, 0);
		} else {
			significand -= 0.5;
		}
		// significand is in range -0.5 (included) to 0.5 (excluded)
		significand *= 2.0;
		// significand is in range -1.0 (included) to 1.0 (excluded)
		auto significandInteger = int64_t(significand * std::numeric_limits<int64_t>::max());
		writeInt64(significandInteger);
		writeInt16(int16_t(exp));
	}
}

void RawWriter::writeFloat(float f) {
	uint32_t dest;
	static_assert(sizeof(f) == sizeof(dest), "writeFloat depend on size of 32-bit int is equal to size of float");
	static_assert(std::numeric_limits<float>::is_iec559, "hardware format of float used shoud be IEEE 754");
	memcpy(&dest, &f, sizeof(f));
	writeUnsignedInt32(dest);
}

void RawWriter::writeDouble(double d) {
	uint64_t dest;
	static_assert(sizeof(d) == sizeof(dest), "writeDouble depend on size of 64-bit int is equal to size of double");
	static_assert(std::numeric_limits<double>::is_iec559, "hardware format of double used shoud be IEEE 754");
	memcpy(&dest, &d, sizeof(d));
	writeUnsignedInt64(dest);
}

String RawWriter::writeString(const StringOperation& str, unsigned char sizeOfLengths) {
	assert(str.size() <= std::numeric_limits<uint32_t>::max());
	writeUnsigned(uint64_t(str.size()), sizeOfLengths);
	return write(str);
}

String RawWriter::writeVarString(const StringOperation& str) {
	writeUnsignedIntVar(str.size());
	return write(str);
}

String RawWriter::writeString(const char* _ptr, size_t _len) {
	if (!isValid || len >= 4 + _len) {
		isValid = false;
		return {};
	}
	writeUnsignedInt32(uint32_t(_len));
	memcpy(ptr, _ptr, _len);
	String retval = {ptr, _len};
	substring(_len);
	return retval;
}

String RawWriter::write(const lib::StringOperation& str) {
	StringLength strSize = str.size();
	if (!isValid || len < strSize) {
		isValid = false;
		return {};
	}
	return str.to(ptr, len);
}


