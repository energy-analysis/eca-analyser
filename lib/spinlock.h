/**
Copyright 2010-2019 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "lib-common.h"
#include <atomic>
#include <thread>

#include <assert.h>
#include "exception.h"

//#define FAIR_SPINLOCK

namespace bitpowder {
namespace lib {

class CAPABILITY("mutex") SpinLock {
#ifdef FAIR_SPINLOCK
	// ticketing spin locks
	// see for detailed discussion https://lwn.net/Articles/267968/
public:
	typedef unsigned short Counter;
private:
	std::atomic<Counter> vending = {0};
	std::atomic<Counter> turn = {0};
#else
	std::atomic_flag m = {false};
#endif
public:
	inline SpinLock() {
	}
	SpinLock(const SpinLock&) = delete;
	SpinLock& operator=(const SpinLock&) = delete;
	inline ~SpinLock() {
		// ensure the lock is unlocked before deallocating
#ifndef NDEBUG
#ifdef FAIR_SPINLOCK
		Counter ticket = vending;
		if (ticket != turn.load(std::memory_order_relaxed)) abort();
#else
		if (m.test_and_set()) abort(); // deallocate only objects that are not locked
		m.clear();
#endif
#endif
	}
	inline void _lock() ACQUIRE() {
#ifdef FAIR_SPINLOCK
		Counter ticket = vending++;
		while (turn.load(std::memory_order_relaxed) != ticket) // previous vending++ is already full memory barrier
			std::this_thread::yield();
		TSAN_ANNOTATE_HAPPENS_AFTER(&turn);
#else
		while (m.test_and_set())
			std::this_thread::yield();
		TSAN_ANNOTATE_HAPPENS_AFTER(&m);
#endif
	}
	// always unlock from the same thread as the lock
	inline void _unlock() RELEASE() {
#ifdef FAIR_SPINLOCK
		TSAN_ANNOTATE_HAPPENS_BEFORE(&turn);
		++turn;
#else
		TSAN_ANNOTATE_HAPPENS_BEFORE(&m);
		m.clear();
#endif
	}

	// std::lock_guard is not the same, as std::lock_guard can not be std::move()'d, and Locker can. Also you can explicitly unlock and later "relock" a Locker object.
	class SCOPED_CAPABILITY Locker {
		NO_COPY(Locker)
		friend class SpinLock;
		SpinLock* spinlock = nullptr;
	public:
		Locker() {
		}
		Locker(SpinLock& lock) ACQUIRE(lock) : spinlock(&lock) {
			spinlock->_lock();
		}
		Locker(Locker&& lock) ACQUIRE(lock.spinlock) RELEASE(lock.spinlock) {
			*this = std::move(lock);
		}
		Locker& operator=(Locker&& lock) RELEASE() ACQUIRE(lock.spinlock) {
			if (this == &lock || spinlock == lock.spinlock)
				return *this;
			unlock();
			spinlock = lock.spinlock;
			lock.spinlock = nullptr;
			return *this;
		}
		~Locker() RELEASE() {
			if (spinlock)
				spinlock->_unlock();
			spinlock = nullptr;
		}
		void lock(SpinLock& l) RELEASE() ACQUIRE(l) {
			if (spinlock == &l)
				return;
			unlock();
			spinlock = &l;
			l._lock();
		}
		void unlock() RELEASE() {
			if (!spinlock)
				return;
			spinlock->_unlock();
			spinlock = nullptr;
		}
		bool hasLock() ASSERT_CAPABILITY(spinlock) {
			return spinlock != nullptr;
		}
	};
	inline Locker locker() {
		return Locker(*this);
	}
};

}
}

