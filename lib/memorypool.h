/**
Copyright 2010-2019 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

/*****
 * MemoryPool
 * - with delayed function execution
 * - fast allocation of temporary memory
 * - fast saving and restoring of the current state
 */

#ifndef __STDCPP_DEFAULT_NEW_ALIGNMENT__
#define __STDCPP_DEFAULT_NEW_ALIGNMENT__ sizeof(double)
#endif

#ifdef MEMORY_DEBUG
#include "memorypool.debug.h"
#else

#include <limits>
#include <cstddef>

#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#include "exception.h"
#include "deque.h"
#include "stack.h"
#include "memory.h"
#include "shared_object.h"

namespace bitpowder {
namespace lib {

struct DelayedItemBase {
	DelayedItemBase* next = nullptr;
	virtual void execute() = 0;
	virtual ~DelayedItemBase() { }
};

template <typename F, typename... T>
struct DelayedItem : public DelayedItemBase {
	F func;
	std::tuple<T...> args;
	DelayedItem(F _f, T... _func) : func(std::move(_f)), args(std::move(_func)...) {
	}
	void execute() {
		std::apply(std::move(func), std::move(args));
	}
};

class MemoryPoolPage {
	friend class MemoryPool;
	template <size_t SIZE>
	friend class StaticMemoryPool;
public:
	MemoryPoolPage* prev = nullptr;
	MemoryPoolPage* next = nullptr;
public:
	MemoryPoolPage(size_t totalSize, bool fromPool, bool deallocate);
private:
	size_t size; // excluding this struct
	size_t available;
	bool fromPool;
	bool deallocate;

	void reset();
	static size_t GetDefaultSize();
	template <size_t AlignOn>
	static inline bool FitsIn(MemoryPoolPage* pool, size_t size, size_t& padding, void*& next) {
		assert(pool);
		char* potentialNextPointer = &GetExtra<char>(pool)[pool->size - pool->available];
		padding = uintptr_t(potentialNextPointer) & (AlignOn - 1);
		padding = (AlignOn - padding) % AlignOn;
		assert(padding < AlignOn);
		next = static_cast<void*>(potentialNextPointer + padding);
		return pool->available >= size + padding;
	}
};

class MemoryPoolStatus {
	friend class MemoryPool;
	MemoryPoolPage* current = nullptr;
	size_t available = 0;
	MemoryPoolPage* nextSmallPage = nullptr;
	MemoryPoolPage* nextLargePage = nullptr;
	DelayedItemBase* delayObjects = nullptr;
public:
	MemoryPoolStatus() {
	}
	MemoryPoolStatus(MemoryPoolPage* _current, size_t _available, MemoryPoolPage* _nextSmallPage, MemoryPoolPage* _nextLargePage, DelayedItemBase* _delayObjects) : current(_current), available(_available), nextSmallPage(_nextSmallPage), nextLargePage(_nextLargePage), delayObjects(_delayObjects) {}
	bool empty() const {
		return current == nullptr && nextSmallPage == nullptr && nextLargePage == nullptr;
	}
};

class MemoryPool {
	NO_COPY(MemoryPool)
protected:
	// NOT: sorted with minimal pools first, checks if space is available until it comes in a larger pool
	// NOT: a way to start searching in pools who have indeeed enough space, by making it an array, and the first item is for bytes < 16, second for < 32, etc
	// because otherwise MemoryPoolStatus struct becomes to large
	Deque<MemoryPoolPage*> all;
	Deque<MemoryPoolPage*> smallPages;
	Deque<MemoryPoolPage*> largePages;
	Stack<DelayedItemBase*> delayObjects; // Delayed items that if executed will release memory. To avoid memory corruption if deallocating
	bool rewinding = false; // busy with rewinding
	MemoryPoolPage* _addMemory(void* data, size_t size);
	void _clear(const MemoryPoolStatus& status);
	void _rewind(const MemoryPoolStatus& status);

	static const size_t MINIMAL_USEFUL_SIZE = 128;
	static const size_t DEFAULT_ALIGN_ON = sizeof(double); //__STDCPP_DEFAULT_NEW_ALIGNMENT__;
	// alloc memory from the current memory pool (alloc new one if there is not enough memory). Only usable in the current autorelease pool
	template <size_t AlignOn = DEFAULT_ALIGN_ON>
	void* _alloc(size_t size) {
		checkCondition(size <= 1 * 1024 * 1024 * 1024); // less as 1 Gb, otherwise probably an error
		if (size == 0 || rewinding)
			return nullptr;
		MemoryPoolPage* current = all.back();
		assert(!current || !current->next);
		void* data = nullptr;
		size_t padding = 0;
		while (!current || !MemoryPoolPage::FitsIn<AlignOn>(current, size, padding, data)) {
			current = allocExtra(size);
		}
		assert(size <= current->available);
		// alloc
		current->available -= size + padding;
		assert(uintptr_t(data) % AlignOn == 0);
		return data;
	}
	template <size_t AlignOn = DEFAULT_ALIGN_ON>
	void* _allocAtLeast(size_t& size) {
		checkCondition(size <= 1 * 1024 * 1024 * 1024); // less as 1 Gb, otherwise probably an error
		if (size == 0 || rewinding)
			return nullptr;
		MemoryPoolPage* current = all.back();
		assert(!current || !current->next);
		void* data = nullptr;
		size_t padding = 0;
		while (!current || !MemoryPoolPage::FitsIn<AlignOn>(current, size, padding, data)) {
			current = allocExtra(size);
		}
		assert(size <= current->available);
		// alloc
		size = current->available - padding;
		current->available = 0;
		assert(uintptr_t(data) % AlignOn == 0);
		return data;
	}
	template <typename T, typename... Args>
	DelayedItem<T, Args...>* _delay(T delayedFunc, Args... args) {
		DelayedItem<T, Args...>* obj = allocItems<DelayedItem<T, Args...>>(1);
		checkAssert(all.back() != nullptr);
		checkAssert(obj != nullptr);
		auto retval = new (static_cast<void*>(obj)) DelayedItem<T, Args...>(std::move(delayedFunc), std::move(args)...);
		return retval;
	}
public:
	template <class Type, size_t AlignOn = std::alignment_of<Type>::value>
	typename std::remove_cv<Type>::type * allocItems(size_t count) {
		return static_cast<typename std::remove_cv<Type>::type*>(_alloc<AlignOn>(count * sizeof(Type)));
	}
	template <class Type, size_t AlignOn = std::alignment_of<Type>::value>
	typename std::remove_cv<Type>::type * allocBytes(size_t bytes) {
		assert(bytes % sizeof(Type) == 0); // should allocate whole blocks
		return static_cast<typename std::remove_cv<Type>::type*>(_alloc<AlignOn>(bytes));
	}
	template <class Type, size_t AlignOn = std::alignment_of<Type>::value>
	typename std::remove_cv<Type>::type * allocAtLeastBytes(size_t& bytes) {
		assert(bytes % sizeof(Type) == 0); // should allocate whole blocks
		return static_cast<typename std::remove_cv<Type>::type*>(_allocAtLeast<AlignOn>(bytes));
	}
	template <typename T, typename... Args>
	void delay(T delayedFunc, Args... args) {
		// FIXME: the DelayedItem object produced by _delay is never trivially destructible (because of virtual destructor). To improve should check in this function if delayedFunc + all args are trivially destructible before adding a destructor
		delayObjects.push_back(manage(_delay(std::move(delayedFunc), std::move(args)...)));
	}
	template <class Type>
	Type* copy(const Type* src, size_t n = 1, typename std::enable_if < std::is_trivially_copyable<Type>::value, Type>::type* = nullptr) {
		if (!src)
			return nullptr;
		auto dst = allocItems<Type>(n);
		memcpy(static_cast<void*>(dst), static_cast<const void*>(src), n * sizeof(Type));
		return dst;
	}
	template <class Type>
	Type* copy(const Type* type, size_t n = 1, typename std::enable_if < !std::is_trivially_copyable<Type>::value, Type >::type* = nullptr) {
		if (!type)
			return nullptr;
		auto dst = allocItems<Type>(n);
		for (auto it = dst; it != dst + n; ++it)
			new (static_cast<void*>(it)) Type(*(type++));
		return dst;
	}
	// this way of construction elements has advantages over new (mp) Type(...), as that one always has an alignment of at least __STDCPP_DEFAULT_NEW_ALIGNMENT__ (in 2017 on this macOS 10.13 64-bits system currently 16 bytes), so allocation of smaller object is are less space efficient.
	template <class Type, typename... Args>
	Type* emplace(Args... args) {
		auto dst = allocItems<Type>(1);
		Type* retval = new (static_cast<void*>(dst)) Type(std::move(args)...);
		return manage(retval);
	}
	// let the memorypool manage this object: the destructor is called automatically
	template <class Type>
	Type* manage(Type* type, typename std::enable_if < !std::is_trivially_destructible<Type>::value, Type >::type* = nullptr) {
		delayObjects.push_back(_delay(destroy<Type>, type));
		return type;
	}
	template <class Type>
	Type* manage(Type* type, typename std::enable_if < std::is_trivially_destructible<Type>::value, Type>::type* = nullptr) {
		return type;
	}
	template <class Object, class Base, class RefCountType, RefCountType Base::*ptr, void (*deleter)(Object*)>
	void retain(const shared_object<Object, Base, RefCountType, ptr, deleter>& ref) {
		// equalvalent to, but uses less memory new (*this, destroy<shared_object<Object, Base, RefCountType, ptr, deleter>>) shared_object<Object, Base, RefCountType, ptr, deleter>(std::move(ref));
		shared_object<Object, Base, RefCountType, ptr, deleter>::IncreaseRefCounter(ref.get());
		delay(shared_object<Object, Base, RefCountType, ptr, deleter>::DecreaseRefCounter, ref.get());
	}

	void* allocAll(size_t& size);

	template <size_t AlignOn = DEFAULT_ALIGN_ON>
	void* calloc(size_t size) {
		void* retval = _alloc<AlignOn>(size);
		bzero(retval, size);
		return retval;
	}

	// do not let memory allocations after this one cross into the next memory pool, restore after with mprestore. Should be done when allocatin a new autoreleasepool
	MemoryPoolStatus status() {
		return {all.back(), all.back() ? all.back()->available : 0, smallPages.back(), largePages.back(), delayObjects.back()};
	}
	// clear storage, it is really returned to the system
	void clear(const MemoryPoolStatus& status) {
		if (empty())
			return;
		_clear(status);
	}
	// rewinds storage, but keep it around for next pass
	void rewind(const MemoryPoolStatus& status) {
		if (empty())
			return;
		_rewind(status);
	}

	class Rewinder {
		MemoryPool* mp;
		MemoryPoolStatus status;
		Rewinder(const Rewinder&) = delete;
		Rewinder& operator=(const Rewinder&) = delete;
	public:
		Rewinder(MemoryPool* _mp, MemoryPoolStatus _status) : mp(_mp), status(_status) {
		}
		Rewinder(Rewinder&& o) : mp(o.mp), status(o.status) {
			o.mp = nullptr;
		}
		Rewinder& operator=(Rewinder&& o) {
			rewind();
			mp = o.mp;
			status = o.status;
			o.mp = nullptr;
			return *this;
		}
		~Rewinder() {
			rewind();
		}
		void rewind() {
			if (mp)
				mp->rewind(status);
			mp = nullptr;
		}
	};
	Rewinder rewinder() {
		return {this, status()};
	}

	size_t available() {
		return all.back() ? all.back()->available : 0;
	}
	void* next() {
		MemoryPoolPage* current = all.back();
		return current ? &GetExtra<char>(current)[current->size - current->available] : nullptr;
	}

	// it is an overestimation, as some memory can be added twice (subrange can be 're-added'), but useful for debugging
	size_t allocatedSizeOverestimation();

	// with spare memory add a memory pool. Do not forget to autorelease if memory is needed for something else!
	MemoryPool(size_t size, void* data);
	MemoryPool();
	MemoryPool(MemoryPool* from, size_t size);
	MemoryPool(MemoryPool& from, size_t size);
	~MemoryPool();
	MemoryPool(MemoryPool&& copy) = default;

	bool empty() {
		return all.empty() && smallPages.empty() && largePages.empty();
	}
	void clear();
	void rewind();

	// true if memory is added (and therefore used)
	bool addMemory(void* data, size_t size);
	void addMemory(MemoryPool* mp);

	char* strdup(const char* str);
	char* strndup(const char* str, size_t length);

	int printf(char** str, size_t max, const char* format, ...) FORMAT(printf, 4, 5);
	int vprintf(char** str, size_t max, const char* format, va_list ap);
	// not terminating the string, useful for generating byte strings
	int printf_nt(char** str, size_t max, const char* format, ...) FORMAT(printf, 4, 5);
	int vprintf_nt(char** str, size_t max, const char* format, va_list ap);

	// true if return succeeded (and the memory is used)
	bool reuse(char* buffer, size_t len);
	bool finishedWith(char* buffer, size_t len);

	void reserve(size_t chunkSize);

	struct Any { };

	template <typename T = Any>
	class Allocator {
		struct Unused {
			Unused* next = nullptr;
			size_t bytes;
			Unused(size_t _bytes) : bytes(_bytes) {
			}
		};
		Stack<Unused*> unused;
	public:
		MemoryPool& mp;
		typedef size_t size_type;
		typedef ptrdiff_t difference_type;
		typedef T* pointer;
		typedef const T* const_pointer;
		typedef T& reference;
		typedef const T& const_reference;
		typedef T value_type;

		template <class U>
		struct rebind {
			typedef Allocator<U> other;
		};

		Allocator(MemoryPool& _mp) : mp(_mp) {
		}

		Allocator(const Allocator& copy) : mp(copy.mp) {
		}
		template <class U>
		Allocator(const Allocator<U>& mpa) : mp(mpa.mp) {
		}

		pointer address(reference x) const {
			return &x;
		}
		const_pointer address(const_reference x) const {
			return &x;
		}
		pointer allocate(size_type n) {
			size_t bytes = sizeof(value_type) * n;
			auto it = unused.begin();
			while (it != unused.end()) {
				if (it->bytes >= bytes)
					return reinterpret_cast<pointer>(it.erase());
				++it;
			}
			return mp.allocBytes<value_type>(bytes);
		}

		void deallocate(pointer p, size_type n) {
			deallocate(static_cast<void*>(p), sizeof(value_type)*n);
		}
		void deallocate(void* p, size_t bytes) {
			if (bytes >= sizeof(Unused) && !mp.rewinding && !mp.finishedWith(static_cast<char*>(p), bytes)) {
				unused.push_back(new(p) Unused(bytes));
			}
		}
		size_type max_size() const {
			return std::numeric_limits<size_type>::max();
		}
		void construct(pointer p,  const_reference val) {
			new(static_cast<void*>(p)) T(val);
		}
		template<class U, typename... Args>
		void construct(U* p,  Args&& ... args) {
			new(static_cast<void*>(p)) U(std::forward<Args>(args)...);
		}
		void construct(pointer p) {
			new(static_cast<void*>(p)) T();
		}
		void destroy(pointer p) {
			p->T::~T();
		}
	};

	Allocator<Any> allocator() {
		return allocator<Any>();
	}
	template <class T>
	Allocator<T> allocator() {
		return Allocator<T>(*this);
	}
private:
	MemoryPoolPage* allocExtra(size_t size);
};

template <class T, class U>
bool operator==(const MemoryPool::Allocator<T>& lhs, const MemoryPool::Allocator<U>& rhs) {
	return &lhs.mp == &rhs.mp;
}

template <class T, class U>
bool operator!=(const MemoryPool::Allocator<T>& lhs, const MemoryPool::Allocator<U>& rhs) {
	return &lhs.mp != &rhs.mp;
}

template <size_t SIZE>
class StaticMemoryPool : public MemoryPool {
	static_assert(SIZE >= MINIMAL_USEFUL_SIZE, "small memorypools are not that useful, consider StringOperations that write directly to a buffer");
#ifndef TSAN_ENABLED // avoid false positives, as stack memory is reused
	static const constexpr size_t BUFFER_SIZE = SIZE + sizeof(MemoryPoolPage);
	alignas(alignof(MemoryPoolPage)) char buffer[BUFFER_SIZE];
#endif
public:
	StaticMemoryPool() : MemoryPool() {
#ifndef TSAN_ENABLED
		addMemory(buffer, BUFFER_SIZE);
#endif
	}
	void clear() {
		MemoryPool::clear();
#ifndef TSAN_ENABLED
		addMemory(buffer, BUFFER_SIZE);
#endif
	}
	void clear(const MemoryPoolStatus& status) {
		MemoryPool::clear(status);
#ifndef TSAN_ENABLED
		if (status.empty())
			addMemory(buffer, BUFFER_SIZE);
#endif
	}
};

class EmbeddedMemoryPool : public MemoryPool {
	char* buffer;
	size_t size;
public:
	EmbeddedMemoryPool(char* _buffer, size_t _size) : buffer(_buffer), size(_size) {
		addMemory(_buffer, size);
	}
	EmbeddedMemoryPool(void* _buffer, size_t _size) : EmbeddedMemoryPool(static_cast<char*>(_buffer), _size) {
	}
	void clear() {
		MemoryPool::clear();
		addMemory(buffer, size);
	}
};

}
}

template <class T>
void* operator new(size_t nbytes, const bitpowder::lib::MemoryPool::Allocator<T>& alloc) {
	return alloc.mp->template allocBytes<char, __STDCPP_DEFAULT_NEW_ALIGNMENT__>(nbytes);
}

inline void* operator new(size_t nbytes, bitpowder::lib::MemoryPool& mp) {
	// because of the ExtraMemory construct below, this allocation should be of type char (to avoid triggering alignment assert in allocBytes)
	return mp.allocBytes<char, __STDCPP_DEFAULT_NEW_ALIGNMENT__>(nbytes);
}

inline void* operator new[](size_t count, bitpowder::lib::MemoryPool& mp) {
	return operator new(count, mp);
}

// do not rely on, as if the wrong order (!) of includes are used, MemoryPool* is cast to void* and a totally different semantic is used
void* operator new(size_t nbytes, bitpowder::lib::MemoryPool* mp) = delete;

template <class T>
void* operator new(size_t nbytes, bitpowder::lib::MemoryPool& mp, void (*f)(T*)) {
	// because of the ExtraMemory construct below, this allocation should be of type char (to avoid triggering alignment assert in allocBytes)
	void* retval = mp.allocBytes<char, std::alignment_of<T>::value>(nbytes);
	mp.delay(f, static_cast<T*>(retval));
	return retval;
}

// do not rely on, as if the wrong order (!) of includes are used, MemoryPool* is cast to void* and a totally different semantic is used
template <class T>
void* operator new(size_t nbytes, bitpowder::lib::MemoryPool* mp, void (*f)(T*)) = delete;

/**
 * @brief Variable size allocations are supported
 *
 * Usages, with extra of size_t:
 * - new(extra) SomeType;
 * - new(extra, MemoryPool) SomeType;
 */

// this wrapper is needed due to 'placement delete' ambiguity in C++14
struct ExtraMemory {
	size_t bytes;
	ExtraMemory(size_t _bytes) : bytes(_bytes) {
	}
	operator size_t() {
		return bytes;
	}
};

template <typename... Args>
void* operator new(size_t nbytes, const ExtraMemory& extra, Args&& ... args) {
	return operator new(nbytes + extra.bytes, std::forward<Args>(args)...);
}

template <typename... Args>
void operator delete(void* p, const ExtraMemory&, Args&& ...) noexcept {
	return operator delete(p);
}

#endif
