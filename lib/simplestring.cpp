/**
Copyright 2010-2018 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "simplestring.h"
#include "hash.h"
#include "rawreader.h"
#include "randombytes.h"
#include "stringparse.h"

/* djb2 algorithm */
/*
unsigned int strhash(const char *str) {
    unsigned char c;
    unsigned int hash = 5381;
    while ((c = *str++))
        hash = ((hash << 5) + hash) ^ c;
    return hash;
    //return *str == 0 ? 1 : *str;
}
*/

/*
const char *base(const char *str) {
    const char *last = str ? strrchr(str, '/') : NULL;
    return last ? &last[1] : str;
}

const char *extension(const char *str) {
    str = base(str);
    const char *last = str ? strrchr(str, '.') : NULL;
    return last ? &last[1] : NULL;
}

char *strsep2(char **s, const char *delim, char *seperator) {
    char *retval = *s;
    StringLength pos = strcspn(*s, delim);
    if (seperator != NULL)
        *seperator = (*s)[pos];
    if ((*s)[pos] == '\0') {
        *s = NULL;
    } else {
        (*s)[pos] = '\0';
        *s = (*s)[pos+1] ? &(*s)[pos+1] : NULL;
    }
    return retval;
}

char asciHexToChar(const char* hex) {
    return digit(hex[0]) << 4 | digit(hex[1]);
}

void unescapeString(char *queryString) {
    if (queryString == NULL)
        return;
    char *src = queryString;
    // faster loop till a % is encounterd
    while (*src != '\0' && *src != '%') {
        if (*src == '+')
            *src = ' ';
        src++;
    }
    char *dst = src;
    while (*src != '\0') {
        // convert '+' to a space
        if (*src == '+')
            *dst = ' ';
        // covert something with % to their real value
        else if (*src == '%' && *(src+1) != 0 && *(src+2) != 0) {
            *dst = digit(src[1]) << 4 | digit(src[2]);
            src+=2;
            // ohterwise copy character
        } else
            *dst = *src;
        src++;
        dst++;
    }
    *dst = '\0';
}
*/

uint8_t FromDigit(char tmp) {
	if (tmp >= 'A' && tmp <= 'F')
		return uint8_t(10 + tmp - 'A');
	else if (tmp >= 'a' && tmp <= 'f')
		return uint8_t(10 + tmp - 'a');
	else if (tmp >= '0' && tmp <= '9')
		return uint8_t(tmp - '0');
	return 0;
}

char ToDigit(uint8_t value) {
	assert(value < 16);
	return "0123456789ABCDEF"[value & 0xFU];
}

namespace bitpowder {
namespace lib {

std::string String::stl() const {
	return std::string(ptr, len);
}

StringParser String::parse() const {
	return {*this};
}

StringContainer String::c() const {
	return StringContainer{op()};
}

StringContainerT String::ct() const {
	return StringContainerT{op()};
}

String::operator std::string() const {
	return this->stl();
}

template <typename T>
T rotate_r(T value, int bits) {
	return (value >> ((-bits) & (sizeof(T) * 8 - 1))) | (value << bits);
}

String String::dir() const {
	String retval = rawdir();
	if (!retval.empty())
		return retval;
	// empty so the part after "/" was empty, this can mean two things:
	// first: /filename (in the root of the filesystem)
	// second: filename (in the current dir)
	if (length() > 0 && ptr[0] == '/')
		return "/";
	return ".";
}

String String::withoutBase() const {
	String retval;
	rspan(!Char<'/'>(), retval);
	return retval;
}

String String::base() const {
	return rsplitOn(Char<'/'>());
}

String String::extension() const {
	String b = base();
	String retval = b.doRSplitOn(Char<'.'>());
	return b.empty() ? String() : retval;
}

String String::trim(bool extra) const {
	String retval;

	static auto space = C(' ');
	static auto extraChars = space + C('\t') + C('\r') + C('\n');
	if (extra) {
		span(extraChars, &retval);
		retval.doRSpan(extraChars);
	} else {
		span(space, &retval);
		retval.doRSpan(space);
	}
	return retval;
}

String String::doSplit(const String& s) {
	return split(s, this);
}

String String::doSplitIgnoreCase(const String& s) {
	return splitIgnoreCase(s, this);
}

String String::doRSplit(const String& s) {
	return rsplit(s, this);
}

String String::doRSplitIgnoreCase(const String& s) {
	return rsplitIgnoreCase(s, this);
}

bool String::contains(const String& str) const {
	String first = split(str);
	return first.length() < length();
}

bool String::containsIgnoreCase(const String& str) const {
	String first = splitIgnoreCase(str);
	return first.length() < length();
}

/*
char String::operator[](off_t off) const {
    if (length() == 0)
        throw std::exception();
    if (off < 0)
        off += length();
    off %= length();
    if (off >= 0)
        return ptr[off];
    return 0;
}
*/


/*
 StringConcat operator+(const StringOperation &a, const StringOperation &b) {
 return StringConcat(a, b);
 }
 */



/*
 StringConstConcat operator+(const DataBase &a, const std::string &b) {
 return StringConstConcat(a, b);
 }

 StringConst2Concat operator+(const std::string &a, const DataBase &b) {
 return StringConst2Concat(a, b);
 }
 */

String String::unescape(Escape escape, MemoryPool& mp) const {
	if (empty())
		return String();
	if (escape == Escape::BACKSLASH)
		return filter(!Esc(C('\\')) + !C('\\'), mp);
	char* dst = mp.allocBytes<char>(len);
	const char* src = ptr;
	unsigned long dst_i = 0;
	unsigned long ptr_i = 0;
	if (escape == Escape::URL) {
		while (ptr_i < len) {
			// convert '+' to a space
			if (src[ptr_i] == '+')
				dst[dst_i] = ' ';
			// covert something with % to their real value
			else if (src[ptr_i] == '%' && ptr_i + 2 < len) {
				dst[dst_i] = char(FromDigit(src[ptr_i + 1]) << 4 | FromDigit(src[ptr_i + 2]));
				ptr_i += 2;
			} else
				dst[dst_i] = src[ptr_i];
			++dst_i;
			++ptr_i;
		}
	} else if (escape == Escape::LikeC) {
		bool previousCharWasEscapeChar = false;
		while (ptr_i < len) {
			char in = src[ptr_i++];
			if (previousCharWasEscapeChar) {
				switch (in) {
				case 'a':
					dst[dst_i++] = '\a';
					break;
				case 'b':
					dst[dst_i++] = '\b';
					break;
				case 'f':
					dst[dst_i++] = '\f';
					break;
				case 'n':
					dst[dst_i++] = '\n';
					break;
				case 'r':
					dst[dst_i++] = '\r';
					break;
				case 't':
					dst[dst_i++] = '\t';
					break;
				case 'v':
					dst[dst_i++] = '\v';
					break;
				case '\\':
					dst[dst_i++] = '\\';
					break;
				case '\'':
					dst[dst_i++] = '\'';
					break;
				case '"':
					dst[dst_i++] = '"';
					break;
				case '?':
					dst[dst_i++] = '?';
					break;
				case '0':
					dst[dst_i++] = '\0';
					break;
				case 'x': {
					// implement hexidecimal encoding like \xhh (with h hex numbers)
					char first = ptr_i < len ? src[ptr_i++] : 0;
					char second = ptr_i < len ? src[ptr_i++] : 0;
					dst[dst_i++] = char(StringUtil::FromHex(first, second));
					break;
				}
				default:
					dst[dst_i++] = in;
				}
				previousCharWasEscapeChar = false;
			} else {
				previousCharWasEscapeChar = in == '\\';
				if (!previousCharWasEscapeChar)
					dst[dst_i++] = in;
			}
		}
	} else if (escape == Escape::JSON) {
		// see spec http://www.ietf.org/rfc/rfc4627.txt; section 2.5:
		bool previousCharWasEscapeChar = false;
		while (ptr_i < len) {
			char in = src[ptr_i++];
			if (previousCharWasEscapeChar) {
				switch (in) {
				case 'b':
					dst[dst_i++] = '\b';
					break;
				case 'f':
					dst[dst_i++] = '\f';
					break;
				case 'n':
					dst[dst_i++] = '\n';
					break;
				case 'r':
					dst[dst_i++] = '\r';
					break;
				case 't':
					dst[dst_i++] = '\t';
					break;
				case '\\':
					dst[dst_i++] = '\\';
					break;
				case '"':
					dst[dst_i++] = '"';
					break;
				case 'u': {
					// implement hexidecimal encoding like \uhhhh (with h hex numbers)
					// proper write out UTF-8 sequences from code points
					unsigned short unicode;
					{
						char first = ptr_i < len ? src[ptr_i++] : 0;
						char second = ptr_i < len ? src[ptr_i++] : 0;
						char third = ptr_i < len ? src[ptr_i++] : 0;
						char fourth = ptr_i < len ? src[ptr_i++] : 0;
						unicode = uint16_t(StringUtil::FromHex(first, second) << 8 | StringUtil::FromHex(third, fourth));
					}
					if (unicode <= 0x7F) {
						dst[dst_i++] = char(unicode & 0x7F);
					} else if (unicode <= 0x7FF) {
						dst[dst_i++] = char(0b11000000 | ((unicode >> 6) & 0b00011111));
						dst[dst_i++] = char(0b10000000 | (unicode & 0b00111111));
					} else if (unicode >= 0xD800 && unicode <= 0xDBFF) {
						// surrogate pair
						if (ptr_i < len + 1 && src[ptr_i] == '\\' && src[ptr_i + 1] == 'u') {
							ptr_i += 2;
							unsigned short unicode2;
							{
								char first = ptr_i < len ? src[ptr_i++] : 0;
								char second = ptr_i < len ? src[ptr_i++] : 0;
								char third = ptr_i < len ? src[ptr_i++] : 0;
								char fourth = ptr_i < len ? src[ptr_i++] : 0;
								unicode2 = uint16_t(StringUtil::FromHex(first, second) << 8 | StringUtil::FromHex(third, fourth));
							}
							unsigned int u = ((unicode & 0x3FFU) << 10 | (unicode2 & 0x3FFU)) + 0x10000U;
							if (u >= 0xFFFF && u < 0x10FFFFFF) {
								dst[dst_i++] = char(0b11110000 | ((u >> 18) & 0b00000111));
								dst[dst_i++] = char(0b10000000 | ((u >> 12) & 0b00111111));
								dst[dst_i++] = char(0b10000000 | ((u >> 6) & 0b00111111));
								dst[dst_i++] = char(0b10000000 | (u & 0b00111111));
							}
						}
					} else {
						dst[dst_i++] = char(0b11100000U | ((unicode >> 12) & 0b00001111U));
						dst[dst_i++] = char(0b10000000U | ((unicode >> 6) & 0b00111111U));
						dst[dst_i++] = char(0b10000000U | (unicode & 0b00111111U));
					}
					break;
				}
				default:
					dst[dst_i++] = in;
				}
				previousCharWasEscapeChar = false;
			} else {
				previousCharWasEscapeChar = in == '\\';
				if (!previousCharWasEscapeChar)
					dst[dst_i++] = in;
			}
		}
	} else if (escape == Escape::SQLITE) {
		bool previousCharWasEscapeChar = false;
		while (ptr_i < len) {
			char in = src[ptr_i++];
			if (previousCharWasEscapeChar) {
				switch (in) {
				case '\'':
					dst[dst_i++] = '\'';
					break;
				default:
					dst[dst_i++] = in;
				}
				previousCharWasEscapeChar = false;
			} else {
				previousCharWasEscapeChar = in == '\'';
				if (!previousCharWasEscapeChar)
					dst[dst_i++] = in;
			}
		}
	} else {
		(*this)(dst, len);
	}
	mp.reuse(&dst[dst_i], len - dst_i);
	return String(dst, dst_i);
}

//a-z, A-Z, 0-9, '-._~/'
String String::escape(Escape esc, MemoryPool& mp) const {
	if (empty())
		return String();
	size_t dstLength = lengthOfEscape(esc);
	char* p = mp.allocBytes<char>(dstLength);
	RawWriter writer = {p, dstLength};
	assert(writer.remaining() == dstLength);
	assert(writer.valid());
	escape(esc, writer);
	assert(writer.remaining() == 0);
	assert(writer.valid());
	return {p, dstLength};
}

void String::escape(Escape escape, RawWriter& writer) const {
	if (escape == Escape::URL) {
		for (auto it = rawbegin(); it != rawend() && writer.valid(); ++it) {
			switch (*it) {
			case '-':
			case '~':
			case '.':
			case '/':
			case ',':
			case '=':
			case '&':
			case ':':
			case '?':
			case '_':
				writer.writeUnsignedByte(*it);
				continue;
			case ' ':
				writer.writeByte('+');
				continue;
			}
			if ('a' <= *it && *it <= 'z') {
				writer.writeUnsignedByte(*it);
				continue;
			}
			if ('A' <= *it && *it <= 'Z') {
				writer.writeUnsignedByte(*it);
				continue;
			}
			if ('0' <= *it && *it <= '9') {
				writer.writeUnsignedByte(*it);
				continue;
			}
			writer.writeByte('%');
			writer.writeByte(ToDigit(*it >> 4));
			writer.writeByte(ToDigit(*it & 0xF));
		}
	} else if (escape == Escape::LikeC) {
		// really convert
		for (auto it = rawbegin(); it != rawend() && writer.valid(); ++it) {
			switch (*it) {
			case '\a':
				writer.write("\\a");
				continue;
			case '\b':
				writer.write("\\b");
				continue;
			case '\f':
				writer.write("\\f");
				continue;
			case '\n':
				writer.write("\\n");
				continue;
			case '\r':
				writer.write("\\r");
				continue;
			case '\t':
				writer.write("\\t");
				continue;
			case '\v':
				writer.write("\\v");
				continue;
			case '\\':
				writer.write("\\\\");
				continue;
			//case '\'': UNNEEDED
			case '"':
				writer.write("\\\"");
				continue;
			//case '?': UNNEEDED
			case '\0':
				writer.write("\\0");
				continue;
			}
			if (*it < 0x20 || *it > 0x7E) {
				writer.write("\\x");
				writer.writeByte(ToDigit(*it >> 4));
				writer.writeByte(ToDigit(*it & 0xF));
				continue;
			}
			writer.writeUnsignedByte(*it);
		}
	} else if (escape == Escape::JSON) {
		// see spec http://www.ietf.org/rfc/rfc4627.txt; section 2.5:
		// > All Unicode characters may be placed within the
		// > quotation marks except for the characters that must be escaped:
		// > quotation mark, reverse solidus, and the control characters (U+0000
		// > through U+001F).
		// this is our starting point; but we avoid ASCII vs UTF-8 stuff by encoding all the characters above 07E as \u1234
		for (auto it = rawbegin(); it != rawend() && writer.valid(); ++it) {
			switch (*it) {
			case '"':
				writer.write("\\\"");
				continue;
			case '\\':
				writer.write("\\\\");
				continue;
			case '\b':
				writer.write("\\b");
				continue;
			case '\f':
				writer.write("\\f");
				continue;
			case '\n':
				writer.write("\\n");
				continue;
			case '\r':
				writer.write("\\r");
				continue;
			case '\t':
				writer.write("\\t");
				continue;
			}
			if (*it < 0x20) { // || *it == 0x7F || (escape == JSON_FROM_BINARY && *it > 0x7E)) {
				writer.write("\\u00");
				writer.writeByte(ToDigit(*it >> 4));
				writer.writeByte(ToDigit(*it & 0xF));
				continue;
			}
			/*
			// not needed by spec
			if (*it & 0x80) {
			    if ((*it & 0xE0) == 0xC0) {
			        // 2 bytes
			        unsigned char first = *it;
			        unsigned char second = it != rawend() ? *++it : 0;
			        //unsigned short unicode = ((first & 0x1C) << 6) | ((first & 0x03) >> 6) | ((second & 0x3F));
			        unsigned int unicode = ((first & 0x1F) << 6) | (second & 0x3F);
			        writer.write("\\u"_S + ToHexString(unicode, precision(4)));
			    } else if ((*it & 0xF0) == 0xE0) {
			        // 3 bytes
			        unsigned char first = *it;
			        unsigned char second = it != rawend() ? *++it : 0;
			        unsigned char third= it != rawend() ? *++it : 0;
			        unsigned int unicode = ((first & 0xF) << 12) | ((second & 0x3F) << 6) | (third & 0x3F);
			        writer.write("\\u"_S + ToHexString(unicode, precision(4)));
			    } else if ((*it & 0xF8) == 0xF0) {
			        // 4 bytes
			        unsigned char first = *it;
			        unsigned char second = it != rawend() ? *++it : 0;
			        unsigned char third = it != rawend() ? *++it : 0;
			        unsigned char fourth = it != rawend() ? *++it : 0;
			        unsigned int unicode = ((first & 0x7) << 18) || ((second & 0x3F) << 12) | ((third & 0x3F) << 6) | (fourth & 0x3F);
			        writer.write("\\u"_S + ToHexString(unicode, precision(4)));
			    } else {
			      std::cout << "wrong UTF encoding" << std::endl;
			    }
			    continue;
			}
			*/
			writer.writeUnsignedByte(*it);
		}
	} else if (escape == Escape::SQLITE) {
		for (auto it = rawbegin(); it != rawend() && writer.valid(); ++it) {
			switch (*it) {
			case '\'':
				writer.write("''");
				continue;
			}
			writer.writeUnsignedByte(*it);
		}
	} else if (escape == Escape::HTML) {
		for (auto it = rawbegin(); it != rawend() && writer.valid(); ++it) {
			switch (*it) {
			case '<':
				writer.write("&lt;");
				continue;
			case '>':
				writer.write("&gt;");
				continue;
			case '&':
				writer.write("&amp;");
				continue;
			}
			writer.writeUnsignedByte(*it);
		}
	}
}

size_t String::lengthOfEscape(String::Escape escape) const {
	size_t extra = 0;
	if (escape == Escape::URL) {
		for (const_raw_iterator it = rawbegin(); it != rawend(); ++it) {
			switch (*it) {
			case '-':
			case '~':
			case '.':
			case '/':
			case ',':
			case '=':
			case '&':
			case ':':
			case '?':
			case '_':
			case ' ':
				continue;
			}
			if ('a' <= *it && *it <= 'z')
				continue;
			if ('A' <= *it && *it <= 'Z')
				continue;
			if ('0' <= *it && *it <= '9')
				continue;
			extra++;
		}
		// allocate
		extra *= 2;
	} else if (escape == Escape::LikeC) {
		// count the needed extra charachters
		for (const_raw_iterator it = rawbegin(); it != rawend(); ++it) {
			switch (*it) {
			case '\a':
			case '\b':
			case '\f':
			case '\n':
			case '\r':
			case '\t':
			case '\v':
			case '\\':
			//case '\'':
			case '"':
			//case '\?':
			case '\0':
				++extra;
				continue;
			}
			if (*it < 0x20 || *it > 0x7E) {
				extra += 3;
				continue;
			}
		}
	} else if (escape == Escape::JSON) {
		// count the needed extra charachters
		for (const_raw_iterator it = rawbegin(); it != rawend(); ++it) {
			switch (*it) {
			case '"':
			case '\\':
			case '\b':
			case '\f':
			case '\n':
			case '\r':
			case '\t':
				++extra;
				continue;
			}
			if (*it < 0x20) { // || *it == 0x7F || (escape == JSON_FROM_BINARY && *it > 0x7E)) {
				extra += 5;
				continue;
			}
		}
	} else if (escape == Escape::SQLITE) {
		// count the needed extra charachters
		for (const_raw_iterator it = rawbegin(); it != rawend(); ++it) {
			switch (*it) {
			case '\'':
				++extra;
				continue;
			}
		}
	} else if (escape == Escape::HTML) {
		// count the needed extra charachters
		for (const_raw_iterator it = rawbegin(); it != rawend(); ++it) {
			switch (*it) {
			case '<':
			case '>':
				extra += 3;
				continue;
			case '&':
				extra += 4;
				continue;
			}
		}
	}
	return len + extra;
}

#define BASE_64_BAD 127
#define BASE_64_PADDING 126
#define BASE_64_HIGHEST 63
#define FROM_BASE_64(x) ( \
    'A' <= (x) && (x) <= 'Z' ? (x) - 'A' : \
    'a' <= (x) && (x) <= 'z' ? (x) - 'a' + 26 : \
    '0' <= (x) && (x) <= '9' ? (x) - '0' + 52 : \
    (x) == '+' ? 62 : \
    (x) == '/' ? 63 : \
    (x) == '=' ? BASE_64_PADDING : /* padding */ \
    BASE_64_BAD \
    )
String String::base64decode(MemoryPool& mp) const {
	StringLength size = length() * 3 / 4;
	char* retval = mp.allocBytes<char>(size);
	StringLength o = 0;
	StringLength i;
	for (i = 0; i + 4 < length(); i += 4) {
		char digit[4];
		for (StringLength j = 0; j < 4; ++j) {
			auto c = ptr[i + j];
			digit[j] = FROM_BASE_64(c);
			if (digit[j] == BASE_64_PADDING || digit[j] == BASE_64_BAD)
				goto end;
		}
		retval[o++] = char(digit[0] << 2 | digit[1] >> 4);
		retval[o++] = char((digit[1] & 0xf) << 4 | digit[2] >> 2);
		retval[o++] = char((digit[2] & 0x3) << 6 | digit[3]);
	}
end:
	if (i < length()) {
		char digit[4];
		for (StringLength j = 0; j < 4; ++j) {
			digit[j] = i + j < length() ? FROM_BASE_64(ptr[i + j]) : BASE_64_BAD;
			if (digit[j] == BASE_64_BAD)
				goto end2;
		}
		if (digit[1] <= BASE_64_HIGHEST) {
			retval[o++] = char(digit[0] << 2 | digit[1] >> 4);
			if (digit[2] <= BASE_64_HIGHEST) {
				retval[o++] = char((digit[1] & 0xf) << 4 | digit[2] >> 2);
				if (digit[3] <= BASE_64_HIGHEST)
					retval[o++] = char((digit[2] & 0x3) << 6 | digit[3]);
			}
		}
	}
end2:
	return String(retval, o);
}

bool String::equalsIgnoreCase(const String& b) const {
	if (size() != b.size())
		return false;
	const char* aIt = begin();
	const char* bIt = b.begin();
	if (aIt == bIt)
		return true;
	while (aIt != end()) {
		if (StringUtil::ToUpper(*aIt) != StringUtil::ToUpper(*bIt))
			return false;
		++aIt;
		++bIt;
	}
	return true;
}

StringContainer StringOperation::c() const {
	return StringContainer{*this};
}

StringContainerT StringOperation::ct() const {
	return StringContainerT{*this};
}

StringLength StringOperation::writeToHelper(char*& buffer, StringLength& bufferLength, const StringOperation& op) {
	StringLength len = op.size();
	len = std::min(bufferLength, len);
	len = op.writeTo(buffer, len);
	buffer += len;
	bufferLength -= len;
	return len;
}

StringLength StringOperation::writeToHelper(char*& buffer, StringLength& bufferLength, const String& str) {
	StringLength len = str.size();
	len = std::min(bufferLength, len);
	len = str.writeTo(buffer, len);
	buffer += len;
	bufferLength -= len;
	return len;
}

StringLength StringOperation::writeToHelper(char*& buffer, StringLength& bufferLength, const StringOperation& op, StringLength max) {
	StringLength len = std::min(op._size, max);
	len = std::min(bufferLength, len);
	len = op.writeTo(buffer, len);
	buffer += len;
	bufferLength -= len;
	return len;
}

StringLength StringOperation::writeToHelper(char*& buffer, StringLength& bufferLength, const String& str, StringLength max) {
	StringLength len = std::min(str.size(), max);
	len = std::min(bufferLength, len);
	len = str.writeTo(buffer, len);
	buffer += len;
	bufferLength -= len;
	return len;
}

StringLength StringOperation::writeToHelper(char*& buffer, StringLength& bufferLength, const void* src, StringLength len) {
	len = std::min(bufferLength, len);
	memcpy(buffer, static_cast<const char*>(src), len);
	buffer += len;
	bufferLength -= len;
	return len;
}

String StringOperation::operator()(MemoryPool& mp) const {
	StringLength l = _size;
	char* buffer = mp.allocBytes<char>(l);
	StringLength actual_length = writeTo(buffer, l);
	if (actual_length < l)
		mp.reuse(buffer + actual_length, l - actual_length);
	return {buffer, actual_length};
}

StringT StringOperation::c_str(MemoryPool& mp) const {
	StringLength l = _size + 1;
	char* buffer = mp.allocBytes<char>(l);
	StringLength actual_length = writeTo(buffer, l - 1);
	buffer[actual_length++] = '\0';
	if (actual_length < l)
		mp.reuse(buffer + actual_length, l - actual_length);
	return {buffer, actual_length - 1};
}

StringOperation::operator std::string() const {
	std::string retval;
	retval.resize(size());
	writeTo(&retval.front(), size());
	return retval;
}

//FROM: http://opensource.apple.com/source/gcc/gcc-5470.3/libiberty/strverscmp.c
/* states: S_N: normal, S_I: comparing integral part, S_F: comparing
           fractional parts, S_Z: idem but with leading Zeroes only */
#define  S_N    0x0
#define  S_I    0x4
#define  S_F    0x8
#define  S_Z    0xC

/* result_type: CMP: return diff; LEN: compare using len_diff/diff */
#define  CMP    2
#define  LEN    3
/* Compare S1 and S2 as strings holding indices/version numbers,
   returning less than, equal to or greater than zero if S1 is less than,
   equal to or greater than S2 (for more info, see the Glibc texinfo doc).  */
int String::compareUsingVersion(const String& rhs) const {
	const char* s1 = pointer();
	const char* s2 = rhs.pointer();

	const unsigned char* p1 = reinterpret_cast<const unsigned char*>(s1);
	const unsigned char* p1end = p1 + size();
	const unsigned char* p2 = reinterpret_cast<const unsigned char*>(s2);
	const unsigned char* p2end = p2 + rhs.size();
	unsigned char c1, c2;
	int state;
	int diff;
	Num isdigit;

	/* Symbol(s)    0       [1-9]   others  (padding)
	     Transition   (10) 0  (01) d  (00) x  (11) -   */
	static const int next_state[] = {
		/* state    x    d    0    - */
		/* S_N */  S_N, S_I, S_Z, S_N,
		/* S_I */  S_N, S_I, S_I, S_I,
		/* S_F */  S_N, S_F, S_F, S_F,
		/* S_Z */  S_N, S_F, S_Z, S_Z
	};

	static const int result_type[] = {
		/* state   x/x  x/d  x/0  x/-  d/x  d/d  d/0  d/-
		                     0/x  0/d  0/0  0/-  -/x  -/d  -/0  -/- */

		/* S_N */  CMP, CMP, CMP, CMP, CMP, LEN, CMP, CMP,
		CMP, CMP, CMP, CMP, CMP, CMP, CMP, CMP,
		/* S_I */  CMP, -1,  -1,  CMP, +1,  LEN, LEN, CMP,
		+1,  LEN, LEN, CMP, CMP, CMP, CMP, CMP,
		/* S_F */  CMP, CMP, CMP, CMP, CMP, LEN, CMP, CMP,
		CMP, CMP, CMP, CMP, CMP, CMP, CMP, CMP,
		/* S_Z */  CMP, +1,  +1,  CMP, -1,  CMP, CMP, CMP,
		-1,  CMP, CMP, CMP
	};

	if (p1 == p2)
		return 0;

	c1 = p1 == p1end ? '\0' : *p1++;
	c2 = *p2++;
	/* Hint: '0' is a digit too.  */
	state = S_N | ((c1 == '0') + (isdigit(char(c1)) != 0));

	while ((diff = c1 - c2) == 0 && c1 != '\0')
		//while ((diff = c1 - c2) == 0 && p1 != p1end)
	{
		state = next_state[state];
		c1 = p1 == p1end ? '\0' : *p1++;
		c2 = p2 == p2end ? '\0' : *p2++;
		state |= (c1 == '0') + isdigit(char(c1));
	}

	state = result_type[state << 2 | (((c2 == '0') + (isdigit(char(c2)) != 0)))];

	switch (state) {
	case CMP:
		return diff;

	case LEN:
		while (isdigit(p1 == p1end ? '\0' : char(*p1++)))
			if (!isdigit(p2 == p2end ? '\0' : char(*p2++)))
				return 1;

		return isdigit(p2 == p2end ? '\0' : char(*p2++)) ? -1 : diff;

	default:
		return state;
	}
}

String String::append(const StringOperation& a, MemoryPool& mp) const {
	String retval = *this;
	if (retval.tryAppend(a, mp))
		return retval;
	return (*this + a)(mp);
}

String String::append(const String& a, MemoryPool& mp) const {
	return append(a.op(), mp);
}

bool String::tryAppend(const StringOperation& a, MemoryPool& mp) {
	const char* next = pointer() + length();
	// next pointer of memory pool is not after string
	if (!empty() && mp.next() != next)
		return false;
	// check enough space is available
	if (mp.available() < a.size())
		return false;
	String retval = a(mp);
	assert(empty() || retval.empty() || retval.pointer() == next);
	if (empty())
		*this = retval;
	else
		len += retval.size();
	return true;
}

bool String::tryAppend(const String& a, MemoryPool& mp) {
	return tryAppend(a.op(), mp);
}

String String::to(char*& buffer, StringLength& size) const {
	StringLength _len = writeTo(buffer, size);
	String retval(buffer, _len);
	buffer += _len;
	size -= _len;
	return retval;
}

size_t String::sizeOfUTF8() const {
	size_t retval = size();
	for (char c : *this)
		retval += static_cast<unsigned char>(c) > 0x7F;
	return retval;
}

String String::toUTF8(MemoryPool& mp) const {
	size_t size = sizeOfUTF8();
	RawWriter writer {size, mp};
	String retval = writer.remainingBytes();
	toUTF8(writer);
	assert(writer.valid());
	assert(writer.remaining() == 0);
	return retval;
}

void String::toUTF8(RawWriter& writer) const {
	for (unsigned char c : raw()) {
		if (c > 0x7F) {
			writer.writeUnsignedByte(0xC0 | (c >> 6));
			writer.writeUnsignedByte(0x80 | (c & 0x3F));
		} else {
			writer.writeUnsignedByte(c);
		}
	}
}

size_t String::sizeOfASCII() const {
	size_t retval = 0;
	for (size_t i = 0; i < len;) {
		unsigned char c = uint8_t(ptr[i]);
		if (c & 0x80) {
			// only accept chars that are convertible to ascii: so U+0000 to U+00FF
			if ((c & 0xE0) == 0xC0) {
				// 2 bytes
				if ((c & 0xFC) == 0xC0) {
					++retval;
				}
				i += 2;
			} else if ((c & 0xF0) == 0xE0) {
				// 3 bytes
				i += 3;
			} else if ((c & 0xF8) == 0xF0) {
				// 4 bytes
				i += 4;
			} else if ((c & 0xFC) == 0xF8) {
				// 5 bytes
				i += 5;
			} else if ((c & 0xFE) == 0xFC) {
				// 6 bytes
				i += 6;
			} else {
				//std::cout << "weird codepoint" << std::endl;
				++i;
			}
		} else {
			++retval;
			i += 1;
		}
	}
	return retval;
}

String String::toASCII(MemoryPool& mp) const {
	size_t size = sizeOfASCII();
	RawWriter writer {size, mp};
	String retval = writer.remainingBytes();
	toASCII(writer);
	assert(writer.valid());
	assert(writer.remaining() == 0);
	return retval;
}

void String::toASCII(RawWriter& writer) const {
	for (size_t i = 0; i < len;) {
		unsigned char c = uint8_t(ptr[i]);
		if (c & 0x80) {
			// only accept chars that are convertible to ascii: so U+0000 to U+00FF
			if ((c & 0xE0) == 0xC0) {
				// 2 bytes
				if ((c & 0xFC) == 0xC0) {
					unsigned char c2 = i + 1 < len ? uint8_t(ptr[i + 1]) : 0;
					writer.writeUnsignedByte(uint8_t((c & 0x3) << 6 | (c2 & 0x3F)));
				}
				i += 2;
			} else if ((c & 0xF0) == 0xE0) {
				// 3 bytes
				i += 3;
			} else if ((c & 0xF8) == 0xF0) {
				// 4 bytes
				i += 4;
			} else if ((c & 0xFC) == 0xF8) {
				// 5 bytes
				i += 5;
			} else if ((c & 0xFE) == 0xFC) {
				// 6 bytes
				i += 6;
			} else {
				std::cout << "weird codepoint: " << ToHexString(c) << std::endl;
				++i;
			}
		} else {
			writer.writeUnsignedByte(c);
			i += 1;
		}
	}
}

template <class T>
StringContainer StringReplace(T&& accumulator, const String& in, const String& toBeReplaced, const String& replaceWith) {
	String suffix;
	String prefix = in.split(toBeReplaced, suffix);
	if (in.size() == prefix.size())
		return (accumulator + in).c();
	return StringReplace(accumulator + prefix + replaceWith, suffix, toBeReplaced, replaceWith);
}

template <class T>
String StringReplace(T&& accumulator, const String& in, const String& toBeReplaced, const String& replaceWith, MemoryPool& mp) {
	String suffix;
	String prefix = in.split(toBeReplaced, suffix);
	if (in.size() == prefix.size())
		return (accumulator + in)(mp);
	return StringReplace(accumulator + prefix + replaceWith, suffix, toBeReplaced, replaceWith, mp);
}

StringContainer String::replace(const String& toBeReplaced, const String& replaceWith) const {
	return StringReplace(""_S, *this, toBeReplaced, replaceWith);
}

String String::replace(const String& toBeReplaced, const String& replaceWith, MemoryPool& mp) const {
	return StringReplace(""_S, *this, toBeReplaced, replaceWith, mp);
}

String String::operator()(char* buffer, StringLength size) const {
	StringLength l = writeTo(buffer, size);
	return String(buffer, l);
}

String bitpowder::lib::StringOperation::operator()(char* buffer, StringLength size) const {
	StringLength l = writeTo(buffer, size);
	return {buffer, l};
}

StringT bitpowder::lib::StringOperation::c_str(char* buffer, StringLength size) const {
	StringLength l = writeTo(buffer, size - 1);
	buffer[l] = '\0';
	return {buffer, l};
}

String bitpowder::lib::StringOperation::to(char*& buffer, StringLength& size) const {
	StringLength l = writeTo(buffer, size);
	String retval (buffer, l);
	buffer += l;
	size -= l;
	return retval;
}

String bitpowder::lib::StringOperation::to(String& str) const {
	StringLength l = writeTo(const_cast<char*>(str.pointer()), str.size());
	String retval = str.substring(0, l);
	str = str.substring(l);
	return retval;
}

StringT bitpowder::lib::String::c_str(MemoryPool& mp) const {
	return op().c_str(mp);
}

String bitpowder::lib::String::operator()(MemoryPool& mp) const {
	return op()(mp);
}

uint64_t GenerateRandomHashSeed() {
	uint64_t retval;
	crypto::RandomBytes(static_cast<void*>(&retval), sizeof(retval));
	return retval;
}

uint64_t StringUtil::HashSeed() {
	static uint64_t seed = GenerateRandomHashSeed();
	return seed;
}

StringHash StringUtil::Hash(const String& s) {
	return hash::xxhash32(s);
}

StringHashShort StringUtil::HashShort(const String& s) {
	return hash::xxhash32(s);
}

StringHashLong StringUtil::HashLong(const String& s) {
	return hash::xxhash64(s);
}

StringHashIgnoreCase StringUtil::HashIgnoreCase(const String& s) {
	return hash::djb2a_uppercase(s);
}

StringStableHashShort StringUtil::StableHashShort(const String& s) {
	return hash::xxhash32(s, 0);
}

StringStableHashLong StringUtil::StableHashLong(const String& s) {
	return hash::xxhash64(s, 0);
}

}
}

