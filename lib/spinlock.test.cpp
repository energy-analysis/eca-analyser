/**
Copyright 2015-2018 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "spinlock.h"
#include <iostream>

IGNORE_WARNINGS_START
#include <gtest/gtest.h>
IGNORE_WARNINGS_END

namespace {
using namespace bitpowder::lib;

TEST(SpinLock, MaxConcurrency) {
#ifdef FAIR_SPINLOCK
	EXPECT_GE(std::numeric_limits<SpinLock::Counter>::max(), std::thread::hardware_concurrency()); // otherwise SpinLocks should use 16/32-bits numbers instead of 8/16-bits numbers
#endif
}
TEST(SpinLock, Basic) {
	SpinLock m;
	{
		auto locker = m.locker();
	}
	SpinLock::Locker locker;
	locker = m.locker();
	SpinLock::Locker locker2 = std::move(locker);
	locker = std::move(locker2);
}

}
