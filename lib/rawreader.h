/**
Copyright 2014-2018 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "simplestring.h"
#include "lib-common.h"
#include <vector>
#include <iterator>

namespace bitpowder {
namespace lib {

template <size_t N>
struct NumberType {
};

template <>
struct NumberType<1> {
	typedef unsigned char u;
	typedef char s;
};
template <>
struct NumberType<2> {
	typedef uint16_t u;
	typedef short s;
};
template <>
struct NumberType<3> {
	typedef uint32_t u;
	typedef int s;
};
template <>
struct NumberType<4> {
	typedef uint32_t u;
	typedef int s;
};
template <>
struct NumberType<5> {
	typedef uint64_t u;
	typedef int64_t s;
};
template <>
struct NumberType<6> {
	typedef uint64_t u;
	typedef int64_t s;
};
template <>
struct NumberType<7> {
	typedef uint64_t u;
	typedef int64_t s;
};
template <>
struct NumberType<8> {
	typedef uint64_t u;
	typedef int64_t s;
};

class RawReader {
	friend class RawWriter;

	static const char* const INVALID;
	const char* begin = INVALID;
	const char* end = nullptr;

 public:
	static const size_t LENGTH_INT_8  = 1;
	static const size_t LENGTH_INT_16 = 2;
	static const size_t LENGTH_INT_32 = 4;
	static const size_t LENGTH_INT_64 = 8;
	static const size_t LENGTH_PLATFORM_INDEPENDANT_DOUBLE = LENGTH_INT_64 + LENGTH_INT_16;
	static const size_t LENGTH_FLOAT = 4;
	static const size_t LENGTH_DOUBLE = 8;
	RawReader() {
	}
	RawReader(const char* _begin, const char* _end) : begin(_begin), end(_end) {
	}
	RawReader(const char* pointer, StringLength length) : begin(pointer), end(pointer + length) {
	}
	RawReader(const lib::String& message) : begin(message.pointer()), end(message.pointer() + message.length()) {
	}
	void set(const lib::String& message) {
		begin = message.pointer();
		end = begin + message.length();
	}
	bool left() const {
		return begin < end;
	}
	bool empty() const {
		return begin == end;
	}
	void limit(size_t remaining) {
		end = std::min(begin + remaining, end);
	}
	size_t remaining() const {
		return valid() ? size_t(end - begin) : 0;
	}
	lib::String remainingBytes() const {
		return String(begin, end);
	}
	static inline bool Valid(const char* begin, const char* end) {
		return begin <= end;
	}
	inline bool valid() const {
		return Valid(begin, end);
	}
	static inline bool ValidToRead(const char*& begin, const char*& end, size_t bytes) {
		if (!Valid(begin, end))
			return false;
		assert(Valid(begin, end));
		// avoid overflow when begin and end are equal to max_pointer
		if (bytes > uintptr_t(end) - uintptr_t(begin)) {
			Invalidate(begin, end);
			return false;
		}
		assert(begin + bytes <= end);
		return true;
	}
	inline bool validToRead(size_t bytes) {
		return ValidToRead(begin, end, bytes);
	}
	static void Invalidate(const char*& begin, const char*& end) {
		end = nullptr;
		begin = end + 1;
	}
	void invalidate() {
		Invalidate(begin, end);
		assert(!valid());
	}

	RawReader readerBytes(size_t bytes) {
		RawReader retval = {readBytes(bytes)};
		if (!valid())
			retval.invalidate();
		return retval;
	}
	// useful to check of a peekSomething() is allowed
	bool checkBounds(size_t size, size_t offset = 0) {
		if (!Valid(begin, end))
			return false;
		if (offset && offset > uintptr_t(end) - uintptr_t(begin)) {
			invalidate();
			return false;
		}
		const char* b = begin + offset;
		if (!Valid(b, end) || size > uintptr_t(end) - uintptr_t(b)) {
			invalidate();
			return false;
		}
		return true;
	}
	lib::String peekBytes(size_t size, size_t offset = 0) {
		if (!checkBounds(size, offset))
			return {};
		const char* b = begin + offset;
		const char* e = b + size;
		return {b, e};
	}
	lib::String readBytes(size_t size) {
		if (!validToRead(size))
			return {};
		const char* e = begin + size;
		lib::String retval = {begin, e};
		begin += size;
		assert(valid());
		return retval;
	}
	void skipBytes(size_t bytes) {
		if (validToRead(bytes))
			begin += bytes;
	}

	// one byte
	char peekByte(size_t offset = 0) {
		if (!checkBounds(LENGTH_INT_8, offset))
			return 0;
		const char* ptr = begin + offset;
		if (ptr > end) {
			invalidate();
			return 0;
		}
		return *ptr;
	}
	inline static char ReadByte(const char*& begin, const char*& end) {
		if (!ValidToRead(begin, end, LENGTH_INT_8))
			return 0;
		const char* ptr = begin;
		begin += LENGTH_INT_8;
		assert(Valid(begin, end));
		return *ptr;
	}
	inline char readByte() {
		return ReadByte(begin, end);
	}
	void skipByte() {
		skipBytes(LENGTH_INT_8);
	}

	unsigned char peekUnsignedByte(size_t offset = 0) {
		if (!checkBounds(LENGTH_INT_8, offset))
			return 0;
		const char* ptr = begin + offset;
		if (ptr > end) {
			invalidate();
			return 0;
		}
		return *reinterpret_cast<const unsigned char*>(ptr);
	}
	inline static unsigned char ReadUnsignedByte(const char*& begin, const char*& end) {
		if (!ValidToRead(begin, end, LENGTH_INT_8))
			return 0;
		const char* ptr = begin;
		begin += LENGTH_INT_8;
		assert(Valid(begin, end));
		return *reinterpret_cast<const unsigned char*>(ptr);
	}
	inline unsigned char readUnsignedByte() {
		return ReadUnsignedByte(begin, end);
	}
	void skipUnsignedByte() {
		skipBytes(LENGTH_INT_8);
	}

	template <typename Type, size_t EXPECTED_SIZE = sizeof(Type)>
	static Type Convert(const char* ptr) {
		static_assert(!std::is_signed<Type>::value, "should be a unsigned data type");
		static_assert(EXPECTED_SIZE <= sizeof(Type), "type size mismatch");
		Type retval = 0;
		memcpy(reinterpret_cast<char*>(&retval) + sizeof(retval) - EXPECTED_SIZE, ptr, EXPECTED_SIZE);
		return ntoh(retval);
	}

	// two bytes
	uint16_t peekUnsignedInt16(size_t offset = 0) {
		if (!checkBounds(LENGTH_INT_16, offset))
			return 0;
		const char* b = begin + offset;
		return Convert<uint16_t, LENGTH_INT_16>(b);
	}
	short peekInt16(size_t offset = 0) {
		uint16_t retval = peekUnsignedInt16(offset);
		return (reinterpret_cast<short&>(retval));
	}
	inline static uint16_t ReadUnsignedInt16(const char*& begin, const char*& end) {
		if (!ValidToRead(begin, end, LENGTH_INT_16))
			return 0;
		const char* ptr = begin;
		begin += LENGTH_INT_16;
		assert(Valid(begin, end));
		return Convert<uint16_t, LENGTH_INT_16>(ptr);
	}
	uint16_t readUnsignedInt16() {
		return ReadUnsignedInt16(begin, end);
	}
	short readInt16() {
		uint16_t retval = readUnsignedInt16();
		return (reinterpret_cast<short&>(retval));
	}
	void skipUnsignedInt16() {
		skipBytes(LENGTH_INT_16);
	}

	// four bytes
	uint32_t peekUnsignedInt32(size_t offset = 0) {
		if (!checkBounds(LENGTH_INT_32, offset))
			return 0;
		const char* b = begin + offset;
		return Convert<uint32_t, LENGTH_INT_32>(b);
	}
	int peekInt32(size_t offset = 0) {
		uint32_t retval = peekUnsignedInt32(offset);
		return (reinterpret_cast<int&>(retval));
	}
	inline static uint32_t ReadUnsignedInt32(const char*& begin, const char*& end) {
		if (!ValidToRead(begin, end, LENGTH_INT_32))
			return 0;
		const char* ptr = begin;
		begin += LENGTH_INT_32;
		assert(Valid(begin, end));
		return Convert<uint32_t, LENGTH_INT_32>(ptr);
	}
	uint32_t readUnsignedInt32() {
		return ReadUnsignedInt32(begin, end);
	}
	int readInt32() {
		uint32_t retval = readUnsignedInt32();
		return (reinterpret_cast<int&>(retval));
	}
	void skipUnsignedInt32() {
		skipBytes(LENGTH_INT_32);
	}

	// 8 bytes
	uint64_t peekUnsignedInt64(size_t offset = 0) {
		if (!checkBounds(LENGTH_INT_64, offset))
			return 0;
		const char* b = begin + offset;
		return Convert<uint64_t, LENGTH_INT_64>(b);
	}
	int64_t peekInt64(size_t offset = 0) {
		uint64_t retval = peekUnsignedInt64(offset);
		return (reinterpret_cast<int64_t&>(retval));
	}
	inline static uint64_t ReadUnsignedInt64(const char*& begin, const char*& end) {
		if (!ValidToRead(begin, end, LENGTH_INT_64))
			return 0;
		const char* ptr = begin;
		begin += LENGTH_INT_64;
		assert(Valid(begin, end));
		return Convert<uint64_t, LENGTH_INT_64>(ptr);
	}
	uint64_t readUnsignedInt64() {
		return ReadUnsignedInt64(begin, end);
	}
	int64_t readInt64() {
		uint64_t retval = readUnsignedInt64();
		return (reinterpret_cast<int64_t&>(retval));
	}
	void skipUnsignedInt64() {
		skipBytes(LENGTH_INT_64);
	}

	// variable length ints
	template <size_t size, typename T = typename NumberType<size>::s>
	T peekSigned() {
		static_assert(std::is_signed<T>::value, "should be a signed data type");
		auto retval = peekUnsigned<size, typename std::make_unsigned<T>::type>();
		return SignExtend<8 * size>(retval);
	}
	template <size_t size, typename T = typename NumberType<size>::s>
	T readSigned() {
		static_assert(std::is_signed<T>::value, "should be a signed data type");
		auto retval = readUnsigned<size, typename std::make_unsigned<T>::type>();
		return SignExtend<8 * size>(retval);
	}
	template <size_t size, typename T = typename NumberType<size>::u>
	T peekUnsigned(size_t offset = 0) {
		static_assert(!std::is_signed<T>::value, "should be a unsigned data type");
		if (!checkBounds(size, offset))
			return 0;
		const char* b = begin + offset;
		return Convert<T, size>(b);
	}
	template <size_t size, typename T = typename NumberType<size>::u>
	static T ReadUnsigned(const char*& begin, const char*& end) {
		static_assert(!std::is_signed<T>::value, "should be a unsigned data type");
		if (!ValidToRead(begin, end, size))
			return 0;
		const char* ptr = begin;
		begin += size;
		assert(Valid(begin, end));
		return Convert<T, size>(ptr);
	}
	template <size_t size, typename T = typename NumberType<size>::u>
	T readUnsigned() {
		return ReadUnsigned<size, T>(begin, end);
	}

	template <typename T = uint64_t>
	static T ReadUnsignedIntVar(const char*& begin, const char*& end) {
		// inspired by unicode. First bits indicate the length:
		// if first bit is 0 -> length is 1 byte
		// if first two bits are 10 -> length is 2 bytes
		// if first three bits are 110 -> length is 3 bytes
		// ...
		// if all 8 bytes are 1's -> length is 9 bytes (so 64-bits int fits)
		unsigned char b = ReadUnsignedByte(begin, end);
		if ((b & 0b10000000) == 0)
			return T(b);
		if (sizeof(T) < 2) {
			begin = end + 1; // mark invalid
			return 0;
		}
		if ((b & 0b11000000) == 0b10000000)
			return T(((b & 0b00111111U) << 8) | ReadUnsignedByte(begin, end));
		if (sizeof(T) < 3) {
			begin = end + 1; // mark invalid
			return 0;
		}
		if ((b & 0b11100000) == 0b11000000)
			return T(((b & 0b00011111U) << 16) | ReadUnsigned<2>(begin, end));
		if (sizeof(T) < 4) {
			begin = end + 1; // mark invalid
			return 0;
		}
		if ((b & 0b11110000) == 0b11100000)
			return T(((b & 0b00001111U) << 24) | ReadUnsigned<3>(begin, end));
		if (sizeof(T) < 5) {
			begin = end + 1; // mark invalid
			return 0;
		}
		if ((b & 0b11111000) == 0b11110000)
			return T(((b & 0b00000111ULL) << 32) | ReadUnsigned<4>(begin, end));
		if (sizeof(T) < 6) {
			begin = end + 1; // mark invalid
			return 0;
		}
		if ((b & 0b11111100) == 0b11111000)
			return T(((b & 0b00000011ULL) << 40) | ReadUnsigned<5>(begin, end));
		if (sizeof(T) < 7) {
			begin = end + 1; // mark invalid
			return 0;
		}
		if ((b & 0b11111110) == 0b11111100)
			return T(((b & 0b00000001ULL) << 48) | ReadUnsigned<6>(begin, end));
		if (b == 0b11111110)
			return T(ReadUnsigned<7>(begin, end));
		if (sizeof(T) < 8) {
			begin = end + 1; // mark invalid
			return 0;
		}
		return T(ReadUnsigned<8>(begin, end));
	}
	template <typename T = uint64_t>
	T readUnsignedIntVar() {
		return ReadUnsignedIntVar<T>(begin, end);
	}

	template <size_t bitsWithData, typename T>
	static typename std::make_signed<T>::type SignExtend(T i) {
		if (bitsWithData == 8 * sizeof(T))
			return *reinterpret_cast<typename std::make_signed<T>::type*>(&i);
		constexpr T check = T(1) << (bitsWithData - 1);
		if ((check & i) == 0)
			return typename std::make_signed<T>::type(i); // positive
		// negative
		i |= ~(check - 1);
		return *reinterpret_cast<typename std::make_signed<T>::type*>(&i);
	}

	int64_t readIntVar() {
		unsigned char b = readUnsignedByte();
		if ((b & 0b10000000) == 0)
			return SignExtend<7>(b);
		if ((b & 0b11000000) == 0b10000000)
			return SignExtend<14>(((b & 0b00111111U) << 8) | readUnsigned<1>());
		if ((b & 0b11100000) == 0b11000000)
			return SignExtend<21>(((b & 0b00011111U) << 16) | readUnsigned<2>());
		if ((b & 0b11110000) == 0b11100000)
			return SignExtend<28>(((b & 0b00001111U) << 24) | readUnsigned<3>());
		if ((b & 0b11111000) == 0b11110000)
			return SignExtend<35>(((b & 0b00000111ULL) << 32) | readUnsigned<4>());
		if ((b & 0b11111100) == 0b11111000)
			return SignExtend<42>(((b & 0b00000011ULL) << 40) | readUnsigned<5>());
		if ((b & 0b11111110) == 0b11111100)
			return SignExtend<49>(((b & 0b00000001ULL) << 48) | readUnsigned<6>());
		if (b == 0b11111110)
			return SignExtend<56>(readUnsigned<7>());
		return readInt64();
	}

	// various

	double readPlatformIndependantDouble();
	void skipPlatformIndependantDouble() {
		skipBytes(LENGTH_PLATFORM_INDEPENDANT_DOUBLE);
	}

	// IEEE 754
	float readFloat();
	void skipFloat() {
		skipBytes(LENGTH_FLOAT);
	}

	// IEEE 754
	double readDouble();
	void skipDouble() {
		skipBytes(LENGTH_DOUBLE);
	}

	//strings
	template <size_t sizeOfLength = 4>
	lib::String peekString(size_t offset = 0) {
		typename NumberType<sizeOfLength>::u length = peekUnsigned<sizeOfLength>(offset);
		return peekBytes(length, offset + sizeOfLength);
	}
	template <size_t sizeOfLength = 4>
	lib::String readString() {
		typename NumberType<sizeOfLength>::u length = readUnsigned<sizeOfLength>();
		return readBytes(length);
	}
	template <size_t sizeOfLength = 4>
	void skipString() {
		typename NumberType<sizeOfLength>::u length = peekUnsigned<sizeOfLength>();
		skipBytes(size_t(sizeOfLength + length));
	}
	lib::String peekVarString(size_t offset = 0) {
		RawReader r = *this;
		r.skipBytes(offset);
		StringLength size (r.readUnsignedIntVar<StringLength>());
		auto retval = r.readBytes(size);
		if (!r.valid())
			invalidate();
		return retval;
	}
	lib::String readVarString() {
		StringLength size (readUnsignedIntVar<StringLength>());
		return readBytes(size);
	}
	void skipVarString() {
		StringLength size (readUnsignedIntVar<StringLength>());
		skipBytes(size);
	}

	// hashed strings
	template <StringStableHashShort (*Hash)(const String& s) = StringUtil::StableHashShort, size_t sizeOfLength = 4>
	lib::Hashed<lib::String, StringStableHashShort, Hash> peekHashedString(size_t offset = 0) {
		StringStableHashShort hash = peekUnsignedInt32(offset);
		return {peekString<sizeOfLength>(offset + LENGTH_INT_32), hash};
	}
	template <StringStableHashShort (*Hash)(const String& s) = StringUtil::StableHashShort, size_t sizeOfLength = 4>
	lib::Hashed<lib::String, StringStableHashShort, Hash> readHashedString() {
		StringStableHashShort hash = readUnsignedInt32();
		return {readString<sizeOfLength>(), hash};
	}
	template <size_t sizeOfLength = 4>
	void skipHashedString() {
		auto length = peekUnsigned<sizeOfLength>(LENGTH_INT_32);
		skipBytes(LENGTH_INT_32 + sizeOfLength + length);
	}
	template <StringStableHashShort (*Hash)(const String& s) = StringUtil::StableHashShort>
	lib::Hashed<lib::String, StringStableHashShort, Hash> readHashedVarString() {
		StringStableHashShort hash = readUnsignedInt32();
		return {readVarString(), hash};
	}
	template <StringStableHashShort (*Hash)(const String& s) = StringUtil::StableHashShort>
	lib::Hashed<lib::String, StringStableHashShort, Hash> peekHashedVarString(size_t offset = 0) {
		StringStableHashShort hash = peekUnsignedInt32(offset);
		return {peekVarString(offset + LENGTH_INT_32), hash};
	}

	// int arrays
	void skipUnsignedInt32Array() {
		uint32_t size = readUnsignedInt32();
		skipBytes(LENGTH_INT_32 * size);
	}
	template <class Allocator = std::allocator<uint32_t>>
	std::vector<uint32_t, Allocator> readUnsignedInt32Array(Allocator && allocator = Allocator()) {
		uint32_t size = readUnsignedInt32();
		std::vector<uint32_t, Allocator> retval(size, std::move(allocator));
		retval.reserve(size);
		while (size-- > 0) {
			retval.push_back(readUnsignedInt32());
		}
		return retval;
	}
	template <class ValueReader>
	class LazyRead : public std::iterator<std::random_access_iterator_tag, typename ValueReader::value_type, size_t> {
		ValueReader data;
		size_t i = 0;
	 public:
		const static size_t Length = ValueReader::Length;
		LazyRead(const ValueReader& _data, size_t _i) : data(_data), i(_i) {
		}
		typename ValueReader::value_type operator*() const {
			return data(i);
		}
		size_t operator-(const LazyRead& rhs) const {
			return i - rhs.i;
		}
		bool operator!=(const LazyRead& rhs) const {
			return i != rhs.i || data != rhs.data;
		}
		LazyRead& operator++() {
			++i;
			return *this;
		}
		LazyRead& operator--() {
			--i;
			return *this;
		}
		LazyRead& operator+=(size_t n) {
			i += n;
			return *this;
		}
	};
	template <class T, size_t LENGTH, T (RawReader::*peek)(size_t offset)>
	struct ValueReader {
		typedef T value_type;
		const static size_t Length = LENGTH;
		static_assert(sizeof(T) == LENGTH, "data type size should match");
		lib::String data;
		T operator()(size_t i) const {
			return (RawReader(data).*peek)(LENGTH * i);
		}
		bool operator!=(const ValueReader& rhs) const {
			return data != rhs.data;
		}
	};
	typedef LazyRead<ValueReader<unsigned char, RawReader::LENGTH_INT_8, &RawReader::peekUnsignedByte>> LazyUnsignedInt8;
	typedef LazyRead<ValueReader<uint32_t, RawReader::LENGTH_INT_32, &RawReader::peekUnsignedInt32>> LazyUnsignedInt32;
	typedef LazyRead<ValueReader<int, RawReader::LENGTH_INT_32, &RawReader::peekInt32>> LazyInt32;
	typedef LazyRead<ValueReader<uint64_t, RawReader::LENGTH_INT_64, &RawReader::peekUnsignedInt64>> LazyUnsignedInt64;
	typedef LazyRead<ValueReader<int64_t, RawReader::LENGTH_INT_64, &RawReader::peekInt64>> LazyInt64;
	template <class T>
	std::pair<T, T> lazyReadArray() {
		uint32_t size = readUnsignedInt32();
		lib::String data = readBytes(size * T::Length);
		return {{{data}, 0}, {{data}, size}};
	}
};

class RawWriter {
	char* ptr;
	StringLength len;
	bool isValid = true;
	void substring(size_t bytes) {
		ptr += bytes;
		len -= bytes;
	}

 public:
	RawWriter() { }
	RawWriter(size_t size, lib::MemoryPool& mp) : ptr(mp.allocBytes<char>(size)), len(size) {
	}
	RawWriter(char* pointer, StringLength length) : ptr(pointer), len(length) {
	}

	RawReader reader(size_t bytes) {
		return isValid && len >= bytes ? RawReader(ptr, StringLength(bytes)) : RawReader();
	}
	lib::StringLength remaining() {
		return valid() ? len : 0;
	}
	lib::String remainingBytes();
	bool valid() {
		return isValid;
	}

	RawWriter writerBytes(size_t bytes);

	void skipBytes(size_t bytes);

	void writeByte(char byte);
	void writeUnsignedByte(uint8_t byte) {
		writeByte(char(byte));
	}
	void writeUnsignedInt16(uint16_t u16);
	void writeInt16(int16_t s16);
	void writeUnsignedInt32(uint32_t u32);
	void writeInt32(int32_t s32);
	void writeUnsignedInt64(uint64_t u64);
	void writeInt64(int64_t s64);
	static unsigned char SizeOfUnsignedIntVar(uint64_t u64);
	static unsigned char SizeOfUnsigned(uint64_t u64);
	void writeUnsigned(uint64_t u64, unsigned char length);
	void writeUnsignedIntVar(uint64_t u64);
	static unsigned char SizeOfIntVar(int64_t i64);
	static unsigned char SizeOfSigned(int64_t i64);
	void writeSigned(int64_t i64, unsigned char length);
	void writeIntVar(int64_t i64);
	void writePlatformIndependantDouble(double d); // double will take 10 bytes!
	void writeFloat(float f);
	void writeDouble(double d);
	lib::String writeString(const lib::StringOperation& str, unsigned char sizeOfLengths = 4);
	template <size_t N>
	lib::String writeString(const char (&str)[N], unsigned char sizeOfLengths = 4) {
		return writeString(String(str), sizeOfLengths);
	}
	lib::String writeVarString(const lib::StringOperation& str);
	template <size_t N>
	lib::String writeVarString(const char (&str)[N]) {
		return writeVarString(String(str));
	}
	static size_t SizeOfHashedString(StringLength len, unsigned char sizeOfLengths = 4) {
		return 4 + sizeOfLengths + len;
	}
	static size_t SizeOfHashedString(const lib::String& s, unsigned char sizeOfLengths = 4) {
		return SizeOfHashedString(s.length(), sizeOfLengths);
	}
	static size_t SizeOfVarString(StringLength len) {
		return SizeOfUnsignedIntVar(len) + len;
	}
	static size_t SizeOfVarString(const lib::String& s) {
		return SizeOfVarString(s.length());
	}
	static size_t SizeOfHashedVarString(StringLength len) {
		return 4 + SizeOfVarString(len);
	}
	static size_t SizeOfHashedVarString(const lib::String& s) {
		return SizeOfHashedVarString(s.length());
	}
	template <StringStableHashShort (*Hash)(const String& s) = StringUtil::StableHashShort>
	lib::Hashed<lib::String, StringStableHashShort, Hash> writeHashedString(const lib::Hashed<lib::String, StringStableHashShort, Hash>& str, unsigned char sizeOfLengths = 4) {
		writeUnsignedInt32(str.hash());
		return {writeString(str, sizeOfLengths), str.hash()};
	}
	template <StringStableHashShort (*Hash)(const String& s) = StringUtil::StableHashShort>
	lib::Hashed<lib::String, StringStableHashShort, Hash> writeHashedVarString(const lib::Hashed<lib::String, StringStableHashShort, Hash>& str) {
		writeUnsignedInt32(str.hash());
		return {writeVarString(str), str.hash()};
	}
	lib::String writeString(const char* ptr, size_t len);
	lib::String write(const lib::StringOperation& str);
	template <size_t N>
	lib::String write(const char (&str)[N]) {
		return write(String(str));
	}
};

template <class Content>
inline RawWriter StringC<Content>::Builder::writer() {
	return {pointer(), length()};
}
#ifdef STRING_TAGGED_POINTER
inline RawWriter StringC<String>::Builder::writer() {
	return {pointer(), length()};
}
#endif

template <class T>
class _RawVarNumberUnsigned : public StringOperation {
 public:
	char header[9];
	_RawVarNumberUnsigned(T _n) : StringOperation(0) {
		RawWriter w = {header, sizeof(header)};
		w.writeUnsignedIntVar(_n);
		_size = sizeof(header) - w.remaining();
		assert(_size == RawWriter::SizeOfUnsignedIntVar(_n));
	}
	virtual StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		StringLength retval = 0;
		retval += StringOperation::writeToHelper(buffer, bufferLength, header, _size);
		return retval;
	}
};
template <class T>
class _RawVarNumberSigned : public StringOperation {
 public:
	char header[9];
	_RawVarNumberSigned(T _n) : StringOperation(0) {
		RawWriter w = {header, sizeof(header)};
		w.writeIntVar(_n);
		_size = sizeof(header) - w.remaining();
		assert(_size == RawWriter::SizeOfIntVar(_n));
	}
	virtual StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		StringLength retval = 0;
		retval += StringOperation::writeToHelper(buffer, bufferLength, header, _size);
		return retval;
	}
};

template <typename T>
_RawVarNumberUnsigned<T> ToRawUnsignedVar(T n) {
	return {n};
}
template <typename T>
_RawVarNumberSigned<T> ToRawSignedVar(T n) {
	return {n};
}

template <class T>
class _RawNumberUnsigned : public StringOperation {
 public:
	char header[8];
	_RawNumberUnsigned(T _n, unsigned char length) : StringOperation(0) {
		RawWriter w = {header, sizeof(header)};
		w.writeUnsigned(_n, length);
		_size = sizeof(header) - w.remaining();
		assert(_size == length);
	}
	virtual StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		StringLength retval = 0;
		retval += StringOperation::writeToHelper(buffer, bufferLength, header, _size);
		return retval;
	}
};
template <class T>
class _RawNumberSigned : public StringOperation {
 public:
	char header[8];
	_RawNumberSigned(T _n, unsigned char length) : StringOperation(0) {
		RawWriter w = {header, sizeof(header)};
		w.writeSigned(_n, length);
		_size = sizeof(header) - w.remaining();
		assert(_size == length);
	}
	virtual StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		StringLength retval = 0;
		retval += StringOperation::writeToHelper(buffer, bufferLength, header, _size);
		return retval;
	}
};

template <typename T>
_RawNumberUnsigned<T> ToRawUnsigned(T n, unsigned char length) {
	return {n, length};
}
template <typename T>
_RawNumberSigned<T> ToRawSigned(T n, unsigned char length) {
	return {n, length};
}

template <class It>
class _RawUnsignedNumberArray : public StringOperation {
 public:
	It begin;
	It end;
	unsigned char length;
	_RawUnsignedNumberArray(It _begin, It _end, unsigned char _length) : begin(_begin), end(_end), length(_length) {
		_size = StringLength(std::distance(begin, end)) * StringLength(length);
	}
	virtual StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		if (bufferLength >= _size) {
			// fast track
			RawWriter w = {buffer, _size};
			for (auto it = begin; it != end; ++it)
				w.writeUnsigned(*it, length);
			return _size;
		}
		StringLength retval = 0;
		for (auto it = begin; it != end; ++it) {
			char header[8];
			RawWriter w = {header, length};
			w.writeUnsigned(*it, length);
			retval += StringOperation::writeToHelper(buffer, bufferLength, header, length);
		}
		return retval;
	}
};

template <typename It>
_RawUnsignedNumberArray<It> ToRawUnsigned(It begin, It end, unsigned char length = 4) {
	return {begin, end, length};
}


template <class T>
class _RawVarString : public StringOperation {
 public:
	T str;
	char header[10];
	StringLength headerLength;
	_RawVarString(const T& _str) : StringOperation(_str.size()), str(_str) {
		checkAssert(_size <= std::numeric_limits<StringLength>::max() - 8);
		RawWriter w = {header, sizeof(header)};
		w.writeUnsignedIntVar(_size);
		headerLength = sizeof(header) - w.remaining();
		_size += headerLength;

	}
	virtual StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		StringLength retval = 0;
		retval += StringOperation::writeToHelper(buffer, bufferLength, header, headerLength);
		retval += StringOperation::writeToHelper(buffer, bufferLength, str);
		return retval;
	}
};

template <class T>
auto ToRawVarString(const T& t) {
	return _RawVarString<decltype(ToString(t))> {ToString(t)};
}

template <size_t SizeOfLengths = 4>
class _RawString : public StringOperation {
	template <int v>
	struct Int2Type {
		enum { value = v };
	};
 public:
	const StringOperation& str;
	_RawString(const StringOperation& _str) : StringOperation(_str.size() + SizeOfLengths), str(_str) {
		checkAssert((_size - SizeOfLengths) <= std::numeric_limits<typename NumberType<SizeOfLengths>::u>::max());
	}
	// if known at compile time, avoid dynamic lookup of correct function (result is improved inlining)
	static void WriteLength(RawWriter& writer, StringLength size, Int2Type<1>) {
		writer.writeByte(char(size));
	}
	static void WriteLength(RawWriter& writer, StringLength size, Int2Type<2>) {
		writer.writeUnsignedInt16(uint16_t(size));
	}
	static void WriteLength(RawWriter& writer, StringLength size, Int2Type<4>) {
		writer.writeUnsignedInt32(uint32_t(size));
	}
	static void WriteLength(RawWriter& writer, StringLength size, Int2Type<8>) {
		writer.writeUnsignedInt64(uint64_t(size));
	}
	template <typename T>
	static void WriteLength(RawWriter& writer, StringLength size, T) {
		writer.writeUnsigned(uint64_t(size), uint8_t(T::value));
	}

	virtual StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		char lengthBuffer[SizeOfLengths];
		RawWriter writer {lengthBuffer, SizeOfLengths};
		assert(writer.remaining() == SizeOfLengths);
		this->WriteLength(writer, _size - SizeOfLengths, Int2Type<SizeOfLengths>());
		assert(writer.valid());
		assert(writer.remaining() == 0);
		StringLength retval = 0;
		retval += StringOperation::writeToHelper(buffer, bufferLength, lengthBuffer, SizeOfLengths);
		retval += StringOperation::writeToHelper(buffer, bufferLength, str);
		return retval;
	}
};

template <size_t SizeOfLengths = 4>
auto ToRawString(const StringOperation& t) {
	return _RawString<SizeOfLengths> {t};
}

template <class T, size_t SizeOfLengths = 4>
class _RawHashedString : public StringOperation {
 public:
	char header[14];
	const T& str;
	_RawHashedString(const T& _str) : StringOperation(_str.string().size() + SizeOfLengths + 4), str(_str) {
		checkAssert((_size - SizeOfLengths - 4) <= std::numeric_limits<typename NumberType<SizeOfLengths>::u>::max());
		RawWriter w = {header, SizeOfLengths + 4};
		w.writeUnsignedInt32(str.hash());
		w.writeUnsigned(str.string().length(), SizeOfLengths);
		assert(w.valid());
		assert(w.remaining() == 0);
	}
	virtual StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		StringLength retval = 0;
		retval += StringOperation::writeToHelper(buffer, bufferLength, header, SizeOfLengths + 4);
		retval += StringOperation::writeToHelper(buffer, bufferLength, str.string());
		return retval;
	}
};

template <size_t SizeOfLengths = 4, class T>
auto ToRawHashedString(const T& t) {
	return _RawHashedString<T, SizeOfLengths> {t};
}


class _RawFloat : public StringOperation {
 public:
	 float value;
	_RawFloat(float _value) : StringOperation(RawReader::LENGTH_FLOAT), value(_value) {
	}
	virtual StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		RawWriter writer {buffer, bufferLength};
		writer.writeFloat(value);
		return bufferLength - writer.remaining();
	}
};

inline auto ToRawFloat(float t) {
	return _RawFloat {t};
}

class _RawDouble : public StringOperation {
 public:
	 double value;
	_RawDouble(double _value) : StringOperation(RawReader::LENGTH_DOUBLE), value(_value) {
	}
	virtual StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		RawWriter writer {buffer, bufferLength};
		writer.writeDouble(value);
		return bufferLength - writer.remaining();
	}
};

inline auto ToRawDouble(double t) {
	return _RawDouble {t};
}

}
}

