/**
Copyright 2014-2019 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <iterator>
#include <tuple>

namespace bitpowder {
namespace lib {

template <typename It>
struct container {
	using iterator = It;
	It b;
	It e;
	auto begin() {
		return b;
	}
	auto end() {
		return e;
	}
	const auto& begin() const {
		return b;
	}
	const auto& end() const {
		return e;
	}
	bool operator<(const container& rhs) const {
		return std::tie(b, e) < std::tie(rhs.b, rhs.e);
	}
	bool empty() const {
		return b == e;
	}
};

template <class It, class F>
class filter_iterator : public std::iterator<std::forward_iterator_tag, typename It::value_type, typename It::difference_type> {
	F func;
	It end;
	It it;
	bool cond = false;
	void progress() {
		while (it != end) {
			cond = func(*it);
			if (cond)
				break;
			++it;
		}
	}
public:
	filter_iterator(It _it, It _end, F&& _func) : func(std::move(_func)), end(std::move(_end)), it(std::move(_it)) {
		progress();
	}
	// if *it is a reference pass as reference; if *it is a value pass as value
	decltype(*it) operator*() { // NOTE: decltype(*it) != auto
		return *it;
	}
  decltype(it.operator->()) operator->() {
    return it.operator->();
  }
	filter_iterator& operator++() {
		++it;
		progress();
		return *this;
	}
	bool operator!=(const filter_iterator& b) {
		return it != b.it;
	}
	bool operator==(const filter_iterator& b) {
		return it == b.it;
	}
	bool operator!=(const It& b) {
		return it != b;
	}
	bool operator==(const It& b) {
		return it == b;
	}
	void insert(const typename It::value_type& a) {
		it.insert(a);
	}
	auto erase() {
		auto retval = std::move(*it);
		it.erase();
		this->progress();
		return retval;
	}
};

template <class It, class F>
filter_iterator<It, F> apply_filter(It begin, It end, F&& filter) {
	return {std::move(begin), std::move(end), std::move(filter)};
}

template <class Container, class F>
auto apply_filter_on(Container&& c, F filter) {
	auto e = apply_filter(c.end(), c.end(), F(filter));
	auto b = apply_filter(c.begin(), c.end(), std::move(filter));
	return container<decltype(b)> {std::move(b), std::move(e)};
}

template <class It, class M>
class map_iterator : public std::iterator<std::forward_iterator_tag, decltype(std::declval<M>().operator()(*std::declval<It>())), typename It::difference_type> {
	M func;
	It it;
	decltype(std::declval<M>().operator()(*std::declval<It>())) value;
public:
	map_iterator(It _it, M _func) : func(std::move(_func)), it(std::move(_it)), value(func(*it)) {
	}
	auto& operator*() {
		return value;
	}
	auto& operator->() {
		return value;
	}
	map_iterator& operator++() {
		++it;
		value = func(*it);
		return *this;
	}
	bool operator!=(const map_iterator& b) {
		return it != b.it;
	}
	bool operator==(const map_iterator& b) {
		return it == b.it;
	}
	bool operator!=(const It& b) {
		return it != b;
	}
	bool operator==(const It& b) {
		return it == b;
	}
	void insert(const typename It::value_type& a) {
		it.insert(a);
	}
	auto erase() {
		auto retval = std::move(value);
		it.erase();
		value = func(*it);
		return retval;
	}
};

template <class It, class F>
map_iterator<It, F> apply_map(It it, F map) {
	return {std::move(it), std::move(map)};
}

template <class Container, class M>
auto apply_map_on(Container&& c, M map) {
	auto e = apply_map(c.end(), M(map));
	auto b = apply_map(c.begin(), std::move(map));
	return container<decltype(b)> {std::move(b), std::move(e)};
}

}
}

