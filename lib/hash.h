/**
Copyright 2016-2017 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "simplestring.h"

extern "C" {
#include "contrib/xxhash.h"
}

namespace bitpowder {
namespace hash {

inline unsigned int xxhash32(const lib::String& data, unsigned int seed = lib::StringUtil::HashSeed()) {
  return XXH32(data.pointer() == nullptr ? "" : data.pointer(), data.length(), seed);
}

inline unsigned long long xxhash64(const lib::String& data, unsigned long long seed = lib::StringUtil::HashSeed()) {
  return XXH64(data.pointer() == nullptr ? "" : data.pointer(), data.length(), seed);
}

template <char (*Transformation)(char c) = lib::StringUtil::ToSame>
constexpr unsigned int _djb2a(const char* str, lib::StringLength length, unsigned int hash) {
  return length == 0 ? hash : _djb2a<Transformation>(&str[1], length - 1, hash * 33 ^ Transformation(str[0]));
}
constexpr unsigned int djb2a(const lib::String& str, unsigned int seed = lib::StringUtil::HashSeed()) {
  return _djb2a(str.pointer(), str.length(), 5381 + seed);
}
constexpr unsigned int djb2a_uppercase(const lib::String& str, unsigned int seed = lib::StringUtil::HashSeed()) {
  return _djb2a<lib::StringUtil::ToUpper>(str.pointer(), str.length(), 5381 + seed);
}

}
}

