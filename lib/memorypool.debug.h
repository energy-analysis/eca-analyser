/*****
 * MemoryPool Debug Version
 * - with delayed function execution
 * - fast allocation of temporary memory
 * - fast saving and restoring of the current state
 *
 * TODO:
 * - make an indirection for pthread specific storage, an struct which get updated, with two members: currentMemoryPool and newPool
 * - also give the current value of the pthread specific storage to every function, it is faster (the function to get the value gets executed multiple times now)
 */
#pragma once

#include <limits>
#include <cstddef>

#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#include <unistd.h>
#include <deque>

#include "memory.h"
#include "shared_object.h"

#define ALIGN_ON sizeof(void*)

namespace bitpowder {
namespace lib {

typedef void (DelayedFunc)(void* data);

class MemoryPoolItem {
	DelayedFunc* func = nullptr;
	void* data = nullptr;
public:
	MemoryPoolItem* next = nullptr;
	friend class MemoryPool;
};

class MemoryPoolStatus {
	MemoryPoolItem* delayed = nullptr;

public:
	MemoryPoolStatus() {
	}
	MemoryPoolStatus(MemoryPoolItem* d) : delayed(d) {
	}
	friend class MemoryPool;
};

class MemoryPool {
	NO_COPY(MemoryPool)
	Stack<MemoryPoolItem*> delayed;
	// alloc memory from the current memory pool (alloc new one if there is not enough memory). Only usable in the current autorelease pool
	void* _alloc(size_t size);
	void* _allocAtLeast(size_t& size);
public:
	template <class Type, size_t AlignOn = std::alignment_of<Type>::value>
	typename std::remove_cv<Type>::type * allocItems(size_t count) {
		return static_cast<typename std::remove_cv<Type>::type*>(_alloc(count * sizeof(Type)));
	}
	template <class Type, size_t AlignOn = std::alignment_of<Type>::value>
	typename std::remove_cv<Type>::type * allocBytes(size_t bytes) {
		assert(bytes % sizeof(Type) == 0); // should allocate whole blocks
		return static_cast<typename std::remove_cv<Type>::type*>(_alloc(bytes));
	}
	template <class Type, size_t AlignOn = std::alignment_of<Type>::value>
	typename std::remove_cv<Type>::type * allocAtLeastBytes(size_t& bytes) {
		assert(bytes % sizeof(Type) == 0); // should allocate whole blocks
		return static_cast<typename std::remove_cv<Type>::type*>(_allocAtLeast(bytes));
	}
	void _delay(DelayedFunc* func, void* object) {
		MemoryPoolItem* pool = new MemoryPoolItem();
		pool->func = func;
		pool->data = object;
		delayed.push_back(pool);
	}
	template <class F>
	struct CallFunc {
		F f;
		CallFunc(F&& _f) : f(std::move(_f)) {
		}
		void operator()() {
			f();
			delete this;
		}
	};
	template <class T, void (*func)(T*)>
	inline void delay(T* obj) {
		_delay(reinterpret_cast<DelayedFunc*>(func), obj);
	}
	template <class T, void (T::*method)()>
	inline void delay(T* obj) {
		void (*func)(T*) = static_cast<void (*)(T*)>(&invoke<T, method>);
		_delay(reinterpret_cast<DelayedFunc*>(func), obj);
	}
	template <typename F>
	void delay(F&& delayedFunc) {
		CallFunc<F>* function = new CallFunc<F>(std::move(delayedFunc));
		delay<CallFunc<F>, &CallFunc<F>::operator()>(function);
	}
	template <typename T, typename... Args>
	void delay(T delayedFunc, Args... args) {
		auto obj = std::bind(delayedFunc, std::forward<Args>(args)...);
		delay<decltype(obj)>(std::move(obj));
	}

	template <class Type>
	Type* copy(const Type* src, size_t n = 1, typename std::enable_if < std::is_trivially_copyable<Type>::value, Type>::type* = nullptr) {
		if (!src)
			return nullptr;
		auto dst = allocItems<Type>(n);
		memcpy(static_cast<void*>(dst), static_cast<const void*>(src), n * sizeof(Type));
		return static_cast<Type*>(dst);
	}
	template <class Type>
	Type* copy(const Type* type, size_t n = 1, typename std::enable_if < !std::is_trivially_copyable<Type>::value, Type >::type* = nullptr) {
		if (!type)
			return nullptr;
		auto dst = allocItems<Type>(n);
		for (auto it = dst; it != dst + n; ++it)
			new (static_cast<void*>(it)) Type(*(type++));
		return dst;
	}
	// this way of construction elements has advantages over new (mp) Type(...), as that one always has an alignment of at least __STDCPP_DEFAULT_NEW_ALIGNMENT__ (in 2017 on this macOS 10.13 64-bits system currently 16 bytes), so allocation of smaller object is are less space efficient.
	template <class Type, typename... Args>
	Type* emplace(Args... args) {
		auto dst = allocItems<Type>(1);
		return new (static_cast<void*>(dst)) Type(std::move(args)...);
	}
	// let the memorypool manage this object: the destructor is called automatically
	template <class Type>
	Type* manage(Type* type) {
		delay(destroy<Type>, type);
		return type;
	}
	template <class Object, class Base = Object, class RefCountType = decltype(Base::refcount), RefCountType Base::*ptr = &Base::refcount, void (*deleter)(Object*) = &performDelete<Object>>
	void retain(shared_object<Object, Base, RefCountType, ptr, deleter> ref) {
		new (*this, destroy<shared_object<Object, Base, RefCountType, ptr, deleter>>) shared_object<Object, Base, RefCountType, ptr, deleter>(std::move(ref));
	}

	void* allocAll(size_t& size);

	void* calloc(size_t size) {
		void* retval = _alloc(size);
#ifndef EXTRA_SAFE
		memset(retval, 0, size);
#endif
		return retval;
	}

	// do not let memory allocations after this one cross into the next memory pool, restore after with mprestore. Should be done when allocatin a new autoreleasepool
	MemoryPoolStatus status();
	void clear(MemoryPoolStatus status);
	void rewind(MemoryPoolStatus status) {
		clear(status);
	}

	class Rewinder {
		MemoryPool* mp;
		MemoryPoolStatus status;
	public:
		Rewinder(Rewinder&& o) : mp(o.mp), status(o.status) {
			o.mp = nullptr;
		}
		~Rewinder() {
			if (mp)
				mp->rewind(status);
		}
		Rewinder(MemoryPool* memoryPool, MemoryPoolStatus _status) : mp(memoryPool), status(_status) {
		}
		void rewind() {
			if (mp)
				mp->rewind(status);
			mp = nullptr;
		}
	};
	Rewinder rewinder() {
		return {this, status()};
	}

	bool reuse(char* buffer [[maybe_unused]], size_t len [[maybe_unused]]) {
		return false;
	}
	bool finishedWith(char* buffer [[maybe_unused]], size_t len [[maybe_unused]]) {
		return false;
	}
	void* next(char alignOn [[maybe_unused]] = ALIGN_ON) {
		return nullptr;
	}

	size_t available() {
		return 0;
	}

	void reserve(size_t size [[maybe_unused]]) {
	}

	size_t allocatedSizeOverestimation() const {
		size_t retval = 0;
		for (auto i : delayed) {
			USING(i);
			++retval;
		}
		return retval;
	}

	// with spare memory add a memory pool. Do not forget to autorelease if memory is needed for something else!
	MemoryPool(size_t size, void* data);
	MemoryPool();
	MemoryPool(MemoryPool* from, size_t size);
	MemoryPool(MemoryPool& from, size_t size);
	~MemoryPool();
	MemoryPool(MemoryPool&& copy) = default;

	void clear();
	void rewind();

	void addMemory(void* data, size_t size);
	void addMemory(MemoryPool* mp);

	// some utility functions that uses memory
	char* strdup(const char* str);
	char* strndup(const char* str, size_t length);
	int printf(char** str, size_t max, const char* format, ...) __attribute__ ((format (printf, 4, 5)));
	int vprintf(char** str, size_t max, const char* format, va_list ap);
	// not terminating the string, useful for generating byte strings
	int printf_nt(char** str, size_t max, const char* format, ...) __attribute__ ((format (printf, 4, 5)));
	int vprintf_nt(char** str, size_t max, const char* format, va_list ap);


	struct Any {};

	template <typename T = Any>
	class Allocator {
	public:
		MemoryPool& mp;
		typedef size_t size_type;
		typedef ptrdiff_t difference_type;
		typedef T* pointer;
		typedef const T* const_pointer;
		typedef T& reference;
		typedef const T& const_reference;
		typedef T value_type;

		template <class U>
		struct rebind {
			typedef Allocator<U> other;
		};

		inline Allocator(MemoryPool& _mp) : mp(_mp) {
		}

		template <class U>
		inline Allocator(const Allocator<U>& mpa) : mp(mpa.mp) {
		}

		inline pointer address(reference x) const {
			return &x;
		}
		inline const_pointer address(const_reference x) const {
			return &x;
		}
		inline pointer allocate(size_type size, const void* hint [[maybe_unused]] = nullptr) {
			return static_cast<pointer>(mp.calloc(sizeof(T) * size));
		}

		inline void deallocate(pointer p [[maybe_unused]], size_type n [[maybe_unused]]) {
		}
		inline void deallocate(void* p [[maybe_unused]], size_type n [[maybe_unused]]) {
		}
		inline size_type max_size() const {
			return std::numeric_limits<size_type>::max();
		}
		void construct(pointer p,  const_reference val) {
			new(reinterpret_cast<void*>(p)) T(val);
		}
		template<class U, typename... Args>
		void construct(U* p,  Args&& ... args) {
			new(static_cast<void*>(p)) U(std::forward<Args>(args)...);
		}
		inline void construct(pointer p) {
			new(reinterpret_cast<void*>(p)) T();
		}
		inline void destroy(pointer p) {
			p->T::~T();
		}
	};

	Allocator<Any> allocator() {
		return allocator<Any>();
	}
	template <class T>
	Allocator<T> allocator() {
		return Allocator<T>(*this);
	}
private:
	void allocExtra();
};

template <class T, class U>
bool operator==(const MemoryPool::Allocator<T>& lhs, const MemoryPool::Allocator<U>& rhs) {
	return &lhs.mp == &rhs.mp;
}

template <class T, class U>
bool operator!=(const MemoryPool::Allocator<T>& lhs, const MemoryPool::Allocator<U>& rhs) {
	return &lhs.mp != &rhs.mp;
}

template <unsigned int SIZE>
class StaticMemoryPool : public MemoryPool {
public:
	StaticMemoryPool() {
	}
	operator MemoryPool* () {
		return this;
	}
	MemoryPool* operator*() {
		return this;
	}
	MemoryPool* operator->() {
		return this;
	}
};

class EmbeddedMemoryPool : public MemoryPool {
public:
	EmbeddedMemoryPool(char* buffer [[maybe_unused]], size_t size [[maybe_unused]]) {
	}
	operator MemoryPool* () {
		return this;
	}
	MemoryPool* operator*() {
		return this;
	}
	MemoryPool* operator->() {
		return this;
	}
};

}
}


template <class T>
inline void* operator new(size_t nbytes, bitpowder::lib::MemoryPool::Allocator<T> alloc) {
	return alloc.mp->alloc(nbytes);
}

inline void* operator new(size_t nbytes, bitpowder::lib::MemoryPool& mp) {
	return mp.allocBytes<char>(nbytes);
}

inline void* operator new[](size_t count, bitpowder::lib::MemoryPool& mp) {
	return operator new(count, mp);
}

// do not rely on, as if the wrong order (!) of includes are used, MemoryPool* is cast to void* and a totally different semantic is used
void* operator new(size_t nbytes, bitpowder::lib::MemoryPool* mp) = delete;

template <class T>
inline void* operator new(size_t nbytes, bitpowder::lib::MemoryPool& mp, void (*f)(T*) = nullptr) {
	void* retval = mp.allocBytes<char>(nbytes);
	mp.delay(reinterpret_cast<bitpowder::lib::DelayedFunc*>(f), retval);
	return  retval;
}

// do not rely on, as if the wrong order (!) of includes are used, MemoryPool* is cast to void* and a totally different semantic is used
template <class T>
inline void* operator new(size_t nbytes, bitpowder::lib::MemoryPool* mp, void (*f)(T*) = nullptr) = delete;


/**
 * @brief Variable size allocations are supported
 *
 * Usages, with extra of size_t:
 * - new(extra) SomeType;
 * - new(extra, MemoryPool) SomeType;
 */

// this wrapper is needed due to 'placement delete' ambiguity in C++14
struct ExtraMemory {
	size_t bytes;
	ExtraMemory(size_t _bytes) : bytes(_bytes) {
	}
	operator size_t() {
		return bytes;
	}
};

template <typename... Args>
void* operator new(size_t nbytes, const ExtraMemory& extra, Args&& ... args) {
	return operator new(nbytes + extra.bytes, std::forward<Args>(args)...);
}

template <typename... Args>
void operator delete(void* p, const ExtraMemory& extra [[maybe_unused]], Args&& ... args [[maybe_unused]]) noexcept {
	return operator delete(p);
}


