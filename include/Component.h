#ifndef COMPONENT_H
#define COMPONENT_H

#include <string>
#include <unordered_set>
#include "EvaluationResult.h"
#include "Value.h"

using namespace std;


class Component
{
public:
    /**
     * @brief evaluateFunction evaluates a function call on this component
     * @param funcName The name of the function
     * @param arg      The value of the argument after evaluation
     * @return         The result of the evaluation: resource used, time and return value
     */
    virtual EvaluationResult evaluateFunction(string funcName, Value* arg) = 0;

    /**
     * @brief dynResCons calculates the time dependent resource consumption of the component in resource/time
     * that is dependent on the dynResCons function of the component model
     * @return The time dependent resource consumption in resource per time unit
     */
    virtual DynResCons dynResCons() = 0;

    /**
     * @return The names of the functions of the component
     */
    virtual unordered_set<string> getComponentFunctionNames() = 0;

    string getName();
    virtual ~Component();
protected:
    string compName;
};

#endif // COMPONENT_H
