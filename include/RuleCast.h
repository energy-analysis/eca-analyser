#ifndef RULECAST_H
#define RULECAST_H

#include <Rule.h>

class RuleCast : public Rule
{
    public:
        RuleCast(const Type& to, Rule* arg, const AST* ast);
        EvaluationResult evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components) override;
        ~RuleCast() override;
    protected:
        vector<Rule*>* getChildren() override;
    private:
        const Type to;
        Rule* arg;
};

#endif // RULECAST_H
