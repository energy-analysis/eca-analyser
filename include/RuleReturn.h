#ifndef RULERETURN_H
#define RULERETURN_H

#include "Rule.h"

class RuleReturn : public Rule
{
    public:
        RuleReturn(Rule* retExpr, const AST* ast);
        EvaluationResult evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components) override;
        ~RuleReturn() override;
    protected:
        vector<Rule*>* getChildren() override;
    private:
        Rule* retExpr;
};

#endif // RULERETURN_H
