#ifndef RULEASSIGN_H
#define RULEASSIGN_H

#include <Rule.h>


class RuleAssign : public Rule
{
    public:
        RuleAssign(string variable, Rule* expression, const AST* ast);
        EvaluationResult evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components) override;
        ~RuleAssign() override;
    protected:
        vector<Rule*>* getChildren() override;
    private:
        string variable;
        Rule* expr;
};

#endif // RULEASSIGN_H
