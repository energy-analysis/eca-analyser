#ifndef LUASIMULATOR_H
#define LUASIMULATOR_H

#include <chrono>
#include "Component.h"
#include "EvaluationResult.h"
#include "lua.hpp"

typedef struct SimulationResult{
    long long time;
    ResourceAmount resource;
    SimulationResult(long long time, ResourceAmount resource) : time(time), resource(resource){

    }
} SimulationResult;


/** LuaSimulator runs the lua program that needs to be analysed from a Lua environment.
 * It registeres the component functions in the Lua state at construction, so they can
 * be called from the environment. It keeps track of the running time and the resource
 * consumed. The simulated running time is determined by the modelled component function
 * call times and the measured time inbetween component function calls. The simulated
 * resource consumption is determined by the modelled incidental resource consumption
 * of component function calls and time dependent resource consumption by modelled
 * dynamic resource consumption.
 *
 *  We need to use a singleton-like pattern here, because the lua closure
 *  needs to call the static function callCompFunc, that needs to access
 *  the components from a static context.
 */
class LuaSimulator
{
public:
    enum Mode {file, code};
    // When given a file name as argument lua, LuaSimulator gives better error messages. A string with the code is more usefull for testing.
    static LuaSimulator* createSimulator(string &lua, Mode mode);
    static LuaSimulator* getInstance();

    void registerComponents(map<string, Component*>* components);
    void declareVar(string name, Value* value);
    SimulationResult run();
    ~LuaSimulator();
private:
    LuaSimulator(string &lua, Mode mode);
    static LuaSimulator* theSimulator;

    DynResCons pause();
    void unpause();
    static int callCompFunc(lua_State* state);

    lua_State* progState;
    map<string, Component*>* components;
    // accumulates total running time
    chrono::nanoseconds duration;
    // last time that unpause() was called
    chrono::time_point<chrono::high_resolution_clock, chrono::nanoseconds> unpauseTime;
    // accumulates total resource consumption
    ResourceAmount resource;
};

#endif // LUASIMULATOR_H
