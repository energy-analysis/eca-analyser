#ifndef FUNCTIONENVIRONMENT_H
#define FUNCTIONENVIRONMENT_H

#include <algorithm>
#include <exception>
#include <fstream>
#include <sstream>
#include "Function.h"
#include "parse.h"

class FunctionEnvironment
{
    public:
        FunctionEnvironment();
        FunctionEnvironment(Time t_input, Time t_const, Time t_var, Time t_assign, Time t_binop, Time t_if,
                Time t_while,
                vector<Function*> functions);
        Time getTInput();
        Time getTConst();
        Time getTVar();
        Time getTAssign();
        Time getTBinop();
        Time getTIf();
        Time getTWhile(); //The time that it takes to jump to the body or over the body after the condition has been evaluated
        Function* getFunction(string name);
        void addFunction(Function* function); //Adds a program function to the environment
        ~FunctionEnvironment();
    private:
        Time t_input, t_const, t_var, t_assign, t_binop, t_if; //t_binop needs to be changed to reflect different binops
        Time t_while; //The time that it takes to jump to the body or over the body after the condition has been evaluated
        vector<Function*> functions;
    protected:
        
};

#endif // FUNCTIONENVIRONMENT_H
