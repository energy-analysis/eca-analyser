#ifndef RULECONST_H
#define RULECONST_H

#include "Rule.h"

class RuleConst : public Rule
{
    public:
        RuleConst(Value* value, const AST* ast);
        EvaluationResult evaluateRule(State&, FunctionEnvironment& env, map<string, Component*>& components) override;
        ~RuleConst() override;
    protected:
        vector<Rule*>* getChildren() override;
    private:
        Value* constValue;
};

#endif // RULECONST_H
