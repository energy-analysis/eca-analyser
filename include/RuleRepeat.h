#ifndef RULEREPEAT_H
#define RULEREPEAT_H

#include "Rule.h"

class RuleRepeat : public Rule
{
    public:
        RuleRepeat(Rule* count, Rule* body, const AST* ast);
        EvaluationResult evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components) override;
        ~RuleRepeat() override;
    protected:
        vector<Rule*>* getChildren() override;
    private:
        Rule* count;
        Rule* body;
};

#endif // RULEREPEAT_H
