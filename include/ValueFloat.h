#ifndef VALUEFLOAT_H
#define VALUEFLOAT_H

#include "Value.h"

class ValueFloat : public Value
{
	public:
                ValueFloat(double value);
                double getValue() const;
                virtual ~ValueFloat();
	private:
		double value;
};

#endif //VALUEFLOAT_H
