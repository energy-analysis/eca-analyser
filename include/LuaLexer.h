#ifndef LUALEXER_H
#define LUALEXER_H

#ifdef __linux__
#define NEWLINE "\n"
#elif _WIN32
#define NEWLINE "\r\n"
#elif _WIN64
#define NEWLINE "\r\n"
#else
#error "OS not supported!" \\TODO
#endif

//#define LUALEXER_DEBUG

#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "parser.h"
#include "stringparse.h"
#include "ValueBool.h"
#include "ValueFloat.h"
#include "ValueInt.h"
#include "ValueString.h"

using namespace bitpowder::lib;

typedef String LuaName;

const static int LUA_LOOKAHEAD = 2;

/* The Lua lexer has been done right directly and lexes all kinds
   of Lua literals and constant values. From the reference:

Keywords:
     and       break     do        else      elseif    end
     false     for       function  goto      if        in
     local     nil       not       or        repeat    return
     then      true      until     while

Other tokens:
     +     -     *     /     %     ^     #
     &     ~     |     <<    >>    //
     ==    ~=    <=    >=    <     >     =
     (     )     {     }     [     ]     ::
     ;     :     ,     .     ..    ...
*/

enum class Literal {
    // 'false' and 'true' are delivered as value tokens
    AND, BREAK, DO, ELSE, ELSEIF, END,
    FOR, FUNCTION, GOTO, IF, IN,
    LOCAL, NIL, NOT, OR, REPEAT, RETURN,
    THEN, UNTIL, WHILE,

    PLUS, MIN, MUL, DIV, MOD, POW, HASH,
    AMPERSAND, NEG, PIPE, BITSHIFT_L, BITSHIFT_R, INT_DIV,
    EQUAL, NOT_EQUAL, LESS_EQ, GREATER_EQ, LESS, GREATER, ASSIGNMENT,
    BRACKET_OPEN, BRACKET_CLOSE, CURLY_OPEN, CURLY_CLOSE, SQUARE_OPEN, SQUARE_CLOSE, DOUBLE_COLON,
    SEMICOLON, COLON, COMMA, DOTS_1, DOTS_2, DOTS_3
};

class LuaLexer {
private:
    MemoryPool& mp;
    String original;
    String c;
    int lookaheadIndex = -1;

    /* All these types need to be different for the parser to check if the token
     * is of the right type
     */
    Token<Literal> literalToken[LUA_LOOKAHEAD];
    Token<LuaName> nameToken[LUA_LOOKAHEAD];
    Token<Value*> constantToken[LUA_LOOKAHEAD];
    Token<Exception> exceptionToken;

    template<char delimiterQuotes>
    struct LuaStringSpanner{
        mutable bool previousCharWasEscapeChar = false;
        mutable bool didSeeEscapeChar = false;
                void reset() const {
                    previousCharWasEscapeChar = false;
                    didSeeEscapeChar = false;
                }
        bool operator()(char in) const {
            bool retval = previousCharWasEscapeChar || in != delimiterQuotes;
            previousCharWasEscapeChar = !previousCharWasEscapeChar && in == '\\';
            didSeeEscapeChar = didSeeEscapeChar || previousCharWasEscapeChar;
            return retval; // true = accept this char
        }
    };

public:
    LuaLexer(String str, MemoryPool& mp) : original(str), c(str), mp(mp) { }

    /*!
     * \brief Reads the next token, skipping newlines and whitespace
     * \return The next token.
     */
    TokenBase *next() {
        // Skip comments
        bool comment = true;
        while(comment){
            StringParser(c).span(Whitespace()).remainder(c);
            if(StringParser(c).accept("--[[").remainder(c)){
                if(!StringParser(c).search("]]").remainder(c)) {
                    exceptionToken = Token<Exception>(Exception("No matching ]] found for multi-line comment"));
                    return &exceptionToken;
                }
            }
            else if(StringParser(c).accept("--").remainder(c)){
                //Skip optional newline
                if(!StringParser(c).search(NEWLINE).remainder(c)){
                    //return nullptr if file does not contain newline
                    c = "";
                    return nullptr;
                }
            }
            else {
                comment = false;
            }
        }
#ifdef LUALEXER_DEBUG
        std::cout << "input after skipping whitespace: <" << c << ">\n";
#endif
        const char *currentPtr = c.pointer();
        TokenBase *retval = recognise();
        if (retval) {
#ifdef LUALEXER_DEBUG
            std::cout << "token: " << retval << " type_hash=" << retval->type << std::endl;
#endif
            retval->start = currentPtr - original.pointer();
            retval->length = c.pointer() - currentPtr;
        }
#ifdef LUALEXER_DEBUG
        std::cout << "Lexer remainder: <" << c << ">\n";
#endif
        return retval;
    }

    /*!
     * \brief Recognizes the token at the beginning of the current string.
     * \return The recognized token.
     */
    TokenBase *recognise() {
        if (c.length() == 0) return nullptr;

        lookaheadIndex = (lookaheadIndex + 1) % LUA_LOOKAHEAD;

        {
            if(StringParser(c).accept("and").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                literalToken[lookaheadIndex] = Literal::AND;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("break").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                literalToken[lookaheadIndex] = Literal::BREAK;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("do").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                literalToken[lookaheadIndex] = Literal::DO;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("elseif").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                literalToken[lookaheadIndex] = Literal::ELSEIF;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("else").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                literalToken[lookaheadIndex] = Literal::ELSE;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("end").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                literalToken[lookaheadIndex] = Literal::END;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("false").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                constantToken[lookaheadIndex] = new ValueBool(false);
                return &constantToken[lookaheadIndex];
            }
            if(StringParser(c).accept("for").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                literalToken[lookaheadIndex] = Literal::FOR;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("function").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                literalToken[lookaheadIndex] = Literal::FUNCTION;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("goto").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                literalToken[lookaheadIndex] = Literal::GOTO;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("if").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                literalToken[lookaheadIndex] = Literal::IF;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("in").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                literalToken[lookaheadIndex] = Literal::IN;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("local").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                literalToken[lookaheadIndex] = Literal::LOCAL;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("nil").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                literalToken[lookaheadIndex] = Literal::NIL;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("not").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                literalToken[lookaheadIndex] = Literal::NOT;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("or").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                literalToken[lookaheadIndex] = Literal::OR;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("repeat").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                literalToken[lookaheadIndex] = Literal::REPEAT;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("return").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                literalToken[lookaheadIndex] = Literal::RETURN;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("then").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                literalToken[lookaheadIndex] = Literal::THEN;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("true").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                constantToken[lookaheadIndex] = new ValueBool(true);
                return &constantToken[lookaheadIndex];
            }
            if(StringParser(c).accept("until").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                literalToken[lookaheadIndex] = Literal::UNTIL;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("while").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)){
                literalToken[lookaheadIndex] = Literal::WHILE;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("<<").remainder(c)){
                literalToken[lookaheadIndex] = Literal::BITSHIFT_L;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept(">>").remainder(c)){
                literalToken[lookaheadIndex] = Literal::BITSHIFT_R;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("//").remainder(c)){
                literalToken[lookaheadIndex] = Literal::INT_DIV;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("==").remainder(c)){
                literalToken[lookaheadIndex] = Literal::EQUAL;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("~=").remainder(c)){
                literalToken[lookaheadIndex] = Literal::NOT_EQUAL;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("<=").remainder(c)){
                literalToken[lookaheadIndex] = Literal::LESS_EQ;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept(">=").remainder(c)){
                literalToken[lookaheadIndex] = Literal::GREATER_EQ;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("::").remainder(c)){
                literalToken[lookaheadIndex] = Literal::DOUBLE_COLON;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("+").remainder(c)){
                literalToken[lookaheadIndex] = Literal::PLUS;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("-").remainder(c)){
                literalToken[lookaheadIndex] = Literal::MIN;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("*").remainder(c)){
                literalToken[lookaheadIndex] = Literal::MUL;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("/").remainder(c)){
                literalToken[lookaheadIndex] = Literal::DIV;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("%").remainder(c)){
                literalToken[lookaheadIndex] = Literal::MOD;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("^").remainder(c)){
                literalToken[lookaheadIndex] = Literal::POW;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("#").remainder(c)){
                literalToken[lookaheadIndex] = Literal::HASH;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("&").remainder(c)){
                literalToken[lookaheadIndex] = Literal::AMPERSAND;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("~").remainder(c)){
                literalToken[lookaheadIndex] = Literal::NEG;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("|").remainder(c)){
                literalToken[lookaheadIndex] = Literal::PIPE;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("<").remainder(c)){
                literalToken[lookaheadIndex] = Literal::LESS;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept(">").remainder(c)){
                literalToken[lookaheadIndex] = Literal::GREATER;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("=").remainder(c)){
                literalToken[lookaheadIndex] = Literal::ASSIGNMENT;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("(").remainder(c)){
                literalToken[lookaheadIndex] = Literal::BRACKET_OPEN;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept(")").remainder(c)){
                literalToken[lookaheadIndex] = Literal::BRACKET_CLOSE;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("{").remainder(c)){
                literalToken[lookaheadIndex] = Literal::CURLY_OPEN;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("}").remainder(c)){
                literalToken[lookaheadIndex] = Literal::CURLY_CLOSE;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("[").remainder(c)){
                literalToken[lookaheadIndex] = Literal::SQUARE_OPEN;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("]").remainder(c)){
                literalToken[lookaheadIndex] = Literal::SQUARE_CLOSE;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept(";").remainder(c)){
                literalToken[lookaheadIndex] = Literal::SEMICOLON;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept(":").remainder(c)){
                literalToken[lookaheadIndex] = Literal::COLON;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept(",").remainder(c)){
                literalToken[lookaheadIndex] = Literal::COMMA;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("...").remainder(c)){
                literalToken[lookaheadIndex] = Literal::DOTS_3;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept("..").remainder(c)){
                literalToken[lookaheadIndex] = Literal::DOTS_2;
                return &literalToken[lookaheadIndex];
            }
            if(StringParser(c).accept(".").remainder(c)){
                literalToken[lookaheadIndex] = Literal::DOTS_1;
                return &literalToken[lookaheadIndex];
            }
        }

        int whole;
        String matched;
        if(StringParser(c)
                .number(whole)
                .accept(".")
                .span(Num())
                .matched(matched)
                .remainder(c)){
            double remainder;
            if(matched.size() == 0){
                exceptionToken = Token<Exception>(Exception("Decimal fraction of floating point constant missing"));
                return &exceptionToken;
            }
            StringParser(matched).number(remainder);
            for(int i = 0; i < matched.size(); i++)
                remainder /= 10;
            constantToken[lookaheadIndex] = new ValueFloat(static_cast<double>(whole) + remainder);
            return &constantToken[lookaheadIndex];
        }

        int i;
        if(StringParser(c)
                .accept("0x")
                .hexNumber(i)
                .remainder(c)){
            constantToken[lookaheadIndex] = new ValueInt(i);
            return &constantToken[lookaheadIndex];
        }
        if(StringParser(c).number(i).remainder(c)){
            constantToken[lookaheadIndex] = new ValueInt(i);
            return &constantToken[lookaheadIndex];
        }

        /* Note: variable names cannot start with a number, but if it were a number, the previous
         * code block would have returned a number token.
         */
        static auto variableName = Alpha() + C('_') + Num();
        if (StringParser(c).span(variableName).matched(nameToken[lookaheadIndex].value).remainder(c)
                && nameToken[lookaheadIndex].value.size() > 0) {
            return &nameToken[lookaheadIndex];
        }

        String s;
        LuaStringSpanner<'"'> doubleQuoteSpanner;
        if (StringParser(c).accept('"').span(doubleQuoteSpanner).matched(s).accept('"').remainder(c)) {
            if (doubleQuoteSpanner.didSeeEscapeChar)
                s = s.unescape(String::Escape::LikeC, mp);
            constantToken[lookaheadIndex] = new ValueString(s.stl());
            return &constantToken[lookaheadIndex];
        }

        LuaStringSpanner<'\''> singleQuoteSpanner;
        if (StringParser(c).accept('\'').span(singleQuoteSpanner).matched(s).accept('\'').remainder(c)) {
            if (singleQuoteSpanner.didSeeEscapeChar)
                s = s.unescape(String::Escape::LikeC, mp);
            constantToken[lookaheadIndex] = new ValueString(s.stl());
            return &constantToken[lookaheadIndex];
        }

        exceptionToken = Token<Exception>(Exception("Error while lexing"));
        return &exceptionToken;
    }

    String getInputString(TokenBase *token) {
        return token ? original.substring(token->start, token->length) : "(none)";
    }

    String remaining() const { return c; }
};

std::ostream& operator<<(std::ostream& os, const Literal& t);

#endif // LUALEXER_H
