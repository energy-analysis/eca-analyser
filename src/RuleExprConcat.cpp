#include "RuleExprConcat.h"
#include "FunctionEnvironment.h"

RuleExprConcat::RuleExprConcat(Rule* left, Rule* right, const AST* ast)
{
    this->left  = left;
    this->right = right;
    this->ast = ast;
}

EvaluationResult RuleExprConcat::evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components)
{
    EvaluationResult l = left->evaluateRule(programState, env, components);
    if(l.getIsRetVal())
        return l;
    EvaluationResult r = right->evaluateRule(programState, env, components);
    return EvaluationResult(
                r.getValue(),
                l.getTime() + r.getTime(),
                l.getResourceAmount() + r.getResourceAmount(),
                r.getIsRetVal()
    );
}

vector<Rule*>* RuleExprConcat::getChildren()
{
    return new vector<Rule*>({left, right});
}

RuleExprConcat::~RuleExprConcat()
{
    //dtor
}
