#include "RuleStmtConcat.h"
#include "FunctionEnvironment.h"

RuleStmtConcat::RuleStmtConcat(Rule* left, Rule* right, const AST* ast)
{
    this->left  = left;
    this->right = right;
    this->ast = ast;
}

EvaluationResult RuleStmtConcat::evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components)
{
	EvaluationResult l = left->evaluateRule(programState, env, components);
	if(l.getIsRetVal())
		return l;
	EvaluationResult r = right->evaluateRule(programState, env, components);
	Value* val = r.getIsRetVal() ? r.getValue() : nullptr;
	return EvaluationResult(val, l.getTime() + r.getTime(), l.getResourceAmount() + r.getResourceAmount(), r.getIsRetVal());
}

vector<Rule*>* RuleStmtConcat::getChildren()
{
	return new vector<Rule*>({left, right});
}

RuleStmtConcat::~RuleStmtConcat()
{
    //dtor
}
