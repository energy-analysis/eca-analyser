#include "RuleWhile.h"
#include "FunctionEnvironment.h"

RuleWhile::RuleWhile(Rule* condition, Rule* body, const AST* ast)
{
    this->condition = condition;
    this->body = body;
    this->ast = ast;
}

EvaluationResult RuleWhile::evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components)
{
    programState.enterLocalScope();
    EvaluationResult e_cond = condition->evaluateRule(programState, env, components);
    programState.exitLocalScope();
    if(e_cond.getIsRetVal())
        return e_cond;
    Time time = e_cond.getTime();
    ResourceAmount resource = e_cond.getResourceAmount();

    ValueBool* valueBool = dynamic_cast<ValueBool*>(e_cond.getValue());
    if(!valueBool)
        throw runtime_error("Exception: Used non-boolean value in statement: \n" + ast->toString(ECA) + "\n");
    time += env.getTWhile();
    resource += td_ec(components, env.getTWhile());

    while(valueBool->getValue() != 0)
    {
        programState.enterLocalScope();
        EvaluationResult e_body = body->evaluateRule(programState, env, components);
        programState.exitLocalScope();
        time += e_body.getTime();
        resource += e_body.getResourceAmount();
        if(e_body.getIsRetVal())
            return ReturnResult(e_body.getValue(), time, resource);

        programState.enterLocalScope();
        e_cond = condition->evaluateRule(programState, env, components);
        programState.exitLocalScope();
        time += e_cond.getTime();
        resource += e_cond.getResourceAmount();
        if(e_cond.getIsRetVal())
            return ReturnResult(e_cond.getValue(), time, resource);

        valueBool = dynamic_cast<ValueBool*>(e_cond.getValue());
        if(!valueBool)
            throw runtime_error("Exception: Used non-boolean value in statement: \n" + ast->toString(ECA) + "\n");
        time += env.getTWhile();
        resource += td_ec(components, env.getTWhile());
    }

    return ValueResult(nullptr, time, resource);
}

vector<Rule*>* RuleWhile::getChildren()
{
    return new vector<Rule*>({condition, body});
}

RuleWhile::~RuleWhile()
{
    //dtor
}
