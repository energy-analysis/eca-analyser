#include "Rule.h"
#include "FunctionEnvironment.h"

const AST* Rule::getAST()
{
    return ast;
}

string Rule::getASTString(ECAOrLua lang)
{
    return ast->toString(lang);
}

ResourceAmount Rule::td_ec(map<string, Component*>& components, Time t)
{
	DynResCons total = 0;
	for(auto pair: components)
		total += pair.second->dynResCons();
	return total * t;
}

Rule::~Rule()
{
    //dtor
}
