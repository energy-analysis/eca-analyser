#include "RuleSkip.h"
#include "FunctionEnvironment.h"

RuleSkip::RuleSkip(const AST* ast)
{
    this->ast = ast;
}

EvaluationResult RuleSkip::evaluateRule(State&, FunctionEnvironment&, map<string, Component*>&)
{
    return ValueResult(nullptr, 0, 0);
}

vector<Rule*>* RuleSkip::getChildren()
{
	return new vector<Rule*>();
}

RuleSkip::~RuleSkip()
{
    //dtor
}
