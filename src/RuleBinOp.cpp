#include "RuleBinOp.h"
#include "FunctionEnvironment.h"

RuleBinOp::RuleBinOp(string binop, Rule* left, Rule* right, const AST* ast)
{
    this->binop = binop;
    this->left = left;
    this->right = right;
    this->ast = ast;
}

EvaluationResult RuleBinOp::evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components)
{
    EvaluationResult l = left->evaluateRule(programState, env, components);
    if(l.getIsRetVal())
        return l;
    EvaluationResult r = right->evaluateRule(programState, env, components);
    if(r.getIsRetVal())
        return ReturnResult(r.getValue(), l.getTime() + r.getTime(), l.getResourceAmount() + r.getResourceAmount());

    Value* v = operate(l.getValue(), r.getValue());

    return ValueResult(
                v,
                l.getTime() + r.getTime() + env.getTBinop(),
                l.getResourceAmount() + r.getResourceAmount() + td_ec(components, env.getTBinop())
    );
}

Value* RuleBinOp::operate(Value* l, Value* r){
    ValueBool* valueBool1 = dynamic_cast<ValueBool*>(l);
    ValueBool* valueBool2 = dynamic_cast<ValueBool*>(r);
    if(valueBool1 && valueBool2)
    {
        int v1 = valueBool1->getValue();
        int v2 = valueBool2->getValue();
        if (binop == "==")
            return new ValueBool(v1 == v2);
        else if (binop == "!=" || binop == "~=")
            return new ValueBool(v1 != v2);
        else if (binop == "and" || binop == "&&")
            return new ValueBool(v1 && v2);
        else if (binop == "or" || binop == "||")
            return new ValueBool(v1 || v2);
        else throw runtime_error("Exception: Invalid binary operator " + binop + " in statement: " + ast->toString(ECA));
    }
    ValueInt* valueInt1 = dynamic_cast<ValueInt*>(l);
    ValueInt* valueInt2 = dynamic_cast<ValueInt*>(r);
    if(valueInt1 && valueInt2)
    {
        int v1 = valueInt1->getValue();
        int v2 = valueInt2->getValue();
        if(binop == "+")
            return new ValueInt(v1 + v2);

        else if(binop == "-")
            return new ValueInt(v1 - v2);

        else if(binop == "*")
            return new ValueInt(v1 * v2);

        else if (binop == ">")
            return new ValueBool(v1 > v2);

        else if (binop == ">=")
            return new ValueBool(v1 >= v2);

        else if (binop == "==")
            return new ValueBool(v1 == v2);

        else if (binop == "!=" || binop == "~=")
            return new ValueBool(v1 != v2);

        else if (binop == "<=")
            return new ValueBool(v1 <= v2);

        else if (binop == "<")
            return new ValueBool(v1 < v2);

        else throw runtime_error("Exception: Invalid binary operator " + binop + " in statement: " + ast->toString(ECA));
    }
    ValueString* valueString1 = dynamic_cast<ValueString*>(l);
    ValueString* valueString2 = dynamic_cast<ValueString*>(r);
    if(valueString1 && valueString2)
    {
        string v1 = valueString1->getValue();
        string v2 = valueString2->getValue();
        if(binop == "..")
            return new ValueString(v1 + v2);
        else if (binop == "==")
        {
            if(v1.compare(v2) == 0)
                return new ValueInt(1);
            else return new ValueInt(0);
        }
        else if (binop == "!=" || binop == "~=")
        {
            if(v1.compare(v2) != 0)
                return new ValueInt(1);
            else return new ValueInt(0);
        }
        else throw runtime_error("Exception: Invalid binary operator" + binop + " in statement: " + ast->toString(ECA));
    }
    else throw runtime_error("Exception: Incompatible types in statement: \n" + ast->toString(ECA) + "\n");
}

vector<Rule*>* RuleBinOp::getChildren()
{
    return new vector<Rule*>({left, right});
}

RuleBinOp::~RuleBinOp()
{
    //dtor
}
