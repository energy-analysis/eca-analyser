//#define PARSER_DEBUG

#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "parse.h"
#include "parser.h"
#include "stringparse.h"

/**
 * TODO:
 * + assignments
 * - comparisons
 * + function calls
 * + function definitions
 * - parsing inputs
 *
 * Future:
 * - negate unary operator
 * - making else optional
 * - ...
 */

using namespace bitpowder::lib;

const static int ECA_LOOKAHEAD = 2;

class ECALexer {
    MemoryPool& mp;
    String original;
    String c;
    int lookaheadIndex = 0;
    Token<String> nameToken[ECA_LOOKAHEAD];
    Token<char> opToken[ECA_LOOKAHEAD];
    Token<Value*> constantToken[ECA_LOOKAHEAD];
    Token<Exception> exceptionToken = Exception("could not Tokenize remaining value");

    void whitespace() {
        static auto space = C(' ') + C('\n') + C('\r') + C('\t');
        StringParser(c).span(space).remainder(c);
    }

    struct CStringSpanner {
        mutable bool previousCharWasEscapeChar = false;
        mutable bool didSeeEscapeChar = false;
				void reset() const {
					previousCharWasEscapeChar = false;
					didSeeEscapeChar = false;
				}
        bool operator()(char in) const {
            bool retval = previousCharWasEscapeChar || in != '"';
            previousCharWasEscapeChar = !previousCharWasEscapeChar && in == '\\';
            didSeeEscapeChar = didSeeEscapeChar || previousCharWasEscapeChar;
            return retval; // true = accept this char
        }
    };

public:
    static const Token<char> SKIP;

    static const Token<char> DOT;

    static const Token<char> MUL;
    static const Token<char> DIV;
    static const Token<char> PLUS;
    static const Token<char> MIN;
    static const Token<char> CONCAT;
    static const Token<char> ASSIGNMENT;
    static const Token<char> COMMA;
    static const Token<char> SEMICOLON;
    static const Token<char> OPEN;
    static const Token<char> CLOSE;

    static const Token<char> AND;
    static const Token<char> OR;

    static const Token<char> LESS;
    static const Token<char> LESS_EQ;
    static const Token<char> EQUAL;
    static const Token<char> NOT_EQUAL;
    static const Token<char> GREATER_EQ;
    static const Token<char> GREATER;

    static const Token<char> IF;
    static const Token<char> THEN;
    static const Token<char> ELSE;

    static const Token<char> BEGIN;
    static const Token<char> END;

    static const Token<char> REPEAT;
    static const Token<char> WHILE;
    static const Token<char> FUNCTION;

    ECALexer(const String &str, MemoryPool& mp) : original(str), c(str), mp(mp) { }

    TokenBase *next() {
        whitespace();
        const char *currentPtr = c.pointer();
        TokenBase *retval = recognise();
        if (retval) {
            //std::cout << "token: " << retval << " type_hash=" << retval->type << std::endl;
            retval->start = currentPtr - original.pointer();
            retval->length = c.pointer() - currentPtr;
        }
        return retval;
    }
    TokenBase *recognise() {
        if (c.length() == 0) return nullptr;

        lookaheadIndex = (lookaheadIndex+1) % ECA_LOOKAHEAD;

        if (StringParser(c).accept("skip").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)) {
            opToken[lookaheadIndex] = SKIP;
            return &opToken[lookaheadIndex];
        }
        if (StringParser(c).accept("if").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)) {
            opToken[lookaheadIndex] = IF;
            return &opToken[lookaheadIndex];
        }
        if (StringParser(c).accept("then").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)) {
            opToken[lookaheadIndex] = THEN;
            return &opToken[lookaheadIndex];
        }
        if (StringParser(c).accept("else").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)) {
            opToken[lookaheadIndex] = ELSE;
            return &opToken[lookaheadIndex];
        }
        if (StringParser(c).accept("function").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)) {
            opToken[lookaheadIndex] = FUNCTION;
            return &opToken[lookaheadIndex];
        }
        if (StringParser(c).accept("begin").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)) {
            opToken[lookaheadIndex] = BEGIN;
            return &opToken[lookaheadIndex];
        }
        if (StringParser(c).accept("end").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)) {
            opToken[lookaheadIndex] = END;
            return &opToken[lookaheadIndex];
        }
        if (StringParser(c).accept("repeat").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)) {
            opToken[lookaheadIndex] = REPEAT;
            return &opToken[lookaheadIndex];
        }
        if (StringParser(c).accept("while").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)) {
            opToken[lookaheadIndex] = WHILE;
            return &opToken[lookaheadIndex];
        }
        if (StringParser(c).accept("and", "&&").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)) {
            opToken[lookaheadIndex] = AND;
            return &opToken[lookaheadIndex];
        }
        if (StringParser(c).accept("or", "||").accept(!(Alpha()+Num()+C('_'))).unskip().remainder(c)) {
            opToken[lookaheadIndex] = OR;
            return &opToken[lookaheadIndex];
        }
        if (StringParser(c).accept("<=").remainder(c)) {
            opToken[lookaheadIndex] = LESS_EQ;
            return &opToken[lookaheadIndex];
        }
        if (StringParser(c).accept(">=").remainder(c)) {
            opToken[lookaheadIndex] = GREATER_EQ;
            return &opToken[lookaheadIndex];
        }
        if (StringParser(c).accept("!=").remainder(c)) {
            opToken[lookaheadIndex] = NOT_EQUAL;
            return &opToken[lookaheadIndex];
        }
        if (StringParser(c).accept("==").remainder(c)) {
            opToken[lookaheadIndex] = EQUAL;
            return &opToken[lookaheadIndex];
        }
        if (StringParser(c).accept(":=").remainder(c)) {
            opToken[lookaheadIndex] = ASSIGNMENT;
            return &opToken[lookaheadIndex];
        }
        if (StringParser(c).accept("..").remainder(c)) {
            opToken[lookaheadIndex] = CONCAT;
            return &opToken[lookaheadIndex];
        }
        if (c[0] == MUL || c[0] == DIV || c[0] == PLUS || c[0] == MIN ||
                c[0] == COMMA || c[0] == OPEN || c[0] == CLOSE || c[0] == SEMICOLON || c[0] == DOT ||
                c[0] == LESS || c[0] == GREATER) {
            opToken[lookaheadIndex].value = c[0];
            c = c.substring(1);
            //std::cout << "Token[lookaheadIndex] op: " << opToken[lookaheadIndex].value << " type=" << opToken[lookaheadIndex].type << std::endl;
            return &opToken[lookaheadIndex];
        }
        int i;
        if (StringParser(c).number(i).remainder(c)) {
            constantToken[lookaheadIndex] = new ValueInt(i);
            return &constantToken[lookaheadIndex];
        }
        static auto variableName = Alpha() + C('_') + Num();
        if (StringParser(c).span(variableName).matched(nameToken[lookaheadIndex].value).remainder(c) &&
                nameToken[lookaheadIndex].value.size() > 0) {
            return &nameToken[lookaheadIndex];
        }
        String s;
        CStringSpanner stringSpanner;
        if (StringParser(c).accept('"').span(stringSpanner).matched(s).accept('"').remainder(c)) {
            // should do unescaping here (as it is known if it is needed)
            if (stringSpanner.didSeeEscapeChar)
                s = s.unescape(String::Escape::LikeC, mp);
            constantToken[lookaheadIndex] = new ValueString(s.stl());
            return &constantToken[lookaheadIndex];
        }
        return &exceptionToken;
    }
    String getInputString(TokenBase *token) {
        return token ? original.substring(token->start, token->length) : "(none)";
    }
    String remaining() const { return c; }
};

const Token<char> ECALexer::SKIP = 's';
const Token<char> ECALexer::DOT = '.';
const Token<char> ECALexer::MUL = '*';
const Token<char> ECALexer::DIV = '/';
const Token<char> ECALexer::PLUS = '+';
const Token<char> ECALexer::MIN = '-';
const Token<char> ECALexer::CONCAT = 'C';
const Token<char> ECALexer::ASSIGNMENT = '=';
const Token<char> ECALexer::COMMA = ',';
const Token<char> ECALexer::SEMICOLON = ';';
const Token<char> ECALexer::OPEN = '(';
const Token<char> ECALexer::CLOSE = ')';
const Token<char> ECALexer::IF = 'i';
const Token<char> ECALexer::THEN = 't';
const Token<char> ECALexer::ELSE = 'e';
const Token<char> ECALexer::BEGIN = 'b';
const Token<char> ECALexer::END = 'd';
const Token<char> ECALexer::REPEAT = 'r';
const Token<char> ECALexer::WHILE = 'w';
const Token<char> ECALexer::FUNCTION = 'f';
const Token<char> ECALexer::LESS = '<';
const Token<char> ECALexer::GREATER = '>';
const Token<char> ECALexer::LESS_EQ = 'l';
const Token<char> ECALexer::GREATER_EQ = 'g';
const Token<char> ECALexer::EQUAL = 'q';
const Token<char> ECALexer::NOT_EQUAL = 'n';
const Token<char> ECALexer::AND = '&';
const Token<char> ECALexer::OR = '|';

template <class Lexer>
struct ECALanguage {
    typedef ASTStatement::Ref Stmt;
    typedef ASTProgram::Ref FuncDefs;

    typedef MemoryPool& UserData;
    typedef Parser<Lexer, ECA_LOOKAHEAD, Stmt, UserData>& PStmt;
    typedef Parser<Lexer, ECA_LOOKAHEAD, FuncDefs, UserData>& PFunDefs;

    /// EXPRESSIONS
    static int name(String& retval, const Token<String>& token, UserData) {
        retval = token.value;
        return 0;
    }

    static int constant(Stmt &value, const Token<Value*>& token, UserData) {
        value = new ASTConstant(token.value);
        return 0;
    }

    static int variable(Stmt &value, const Token<String> &token, UserData) {
        value = new ASTVariable(token.value);
        return 0;
    }

    static PStmt parenthesis(PStmt cont) {
        return cont().debug("parenthesis").perform(ECALexer::OPEN, stmt, ECALexer::CLOSE);
    }

    static PStmt functionCall(PStmt cont) {
        String functionName;
        return cont()
            .debug("functionCall")
            .fetch(name, functionName)
            .accept(ECALexer::OPEN)
            .perform(stmt)
            .modify([](Stmt &a, const String& functionName, UserData) {
                if (!a->isExpression())
                    return -1;
                a = new ASTFunctionCall(functionName, a);
                return 0;
            }, functionName)
            .accept(ECALexer::CLOSE);
    }

    static PStmt componentFunctionCall(PStmt cont) {
        String componentName;
        String functionName;
        return cont()
            .debug("componentFunctionCall")
            .fetch(name, componentName)
            .accept(ECALexer::DOT)
            .fetch(name, functionName)
            .accept(ECALexer::OPEN)
            .perform(stmt)
            .modify([](Stmt &a, const String& componentName, const String& functionName, UserData) {
                if (!a->isExpression())
                    return -1;
                a = new ASTComponentFunctionCall(componentName, functionName, a);
                return 0;
            }, componentName, functionName)
            .accept(ECALexer::CLOSE);
    }

    static PStmt primary(PStmt cont) {
        return cont().debug("primary"_S).choose(functionCall, componentFunctionCall, variable, constant, parenthesis);

    }

    static PStmt mulOp(PStmt cont) {
        return cont().accept(ECALexer::MUL).process(primary, [](Stmt &a, Stmt &&b, UserData) {
            if (!a->isExpression() || !b->isExpression())
                return -1;
            a = new ASTBinary({a, "*", b});
            return 0;
        });
    }

    static PStmt divOp(PStmt cont) {
        return cont().accept(ECALexer::DIV).process(primary, [](Stmt &a, Stmt &&b, UserData) {
            if (!a->isExpression() || !b->isExpression())
                return -1;
            a = new ASTBinary({a, "/", b});
            return 0;
        });
    }

    static PStmt multiplication(PStmt cont) {
        return cont().debug("multiplication").perform(primary).repeatChoose(mulOp, divOp);
    }

    static PStmt plusOp(PStmt cont) {
        return cont().accept(ECALexer::PLUS).process(multiplication, [](Stmt &a, Stmt &&b, UserData) {
            if (!a->isExpression() || !b->isExpression())
                return -1;
            a = new ASTBinary({a, "+", b});
            return 0;
        });
    }

    static PStmt minOp(PStmt cont) {
        return cont().accept(ECALexer::MIN).process(multiplication, [](Stmt &a, Stmt &&b, UserData) {
            if (!a->isExpression() || !b->isExpression())
                return -1;
            a = new ASTBinary({a, "-", b});
            return 0;
        });
    }

    static PStmt addition(PStmt cont) {
        return cont().debug("addition"_S).perform(multiplication).repeatChoose(plusOp, minOp);
    }

    static PStmt opConcatTail(PStmt cont) {
        return cont().accept(ECALexer::CONCAT).process(stringConcat, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTBinary(a, "..", b);
            return 0;
        });
    }

    //String concatenation is parsed right-associatively as in Lua
    static PStmt stringConcat(PStmt cont) {
        return cont().debug("stringConcat"_S).perform(addition).opt(opConcatTail);
    }

    static PStmt assignment(PStmt cont) {
        String variableName;
        return cont().debug("assignment"_S).fetch(name, variableName).accept(ECALexer::ASSIGNMENT).perform(expr).modify([](Stmt& a, String& variableName, UserData) {
            a = new ASTAssignment(variableName, a, AssignmentSort::Unknown);
            return 0;
        }, variableName);
    }

    static PStmt exprBase(PStmt cont) {
        return cont().debug("exprBase"_S).choose(assignment, stringConcat);
    }

    static PStmt equalOp(PStmt cont) {
        return cont().accept(ECALexer::EQUAL)
                .process(exprBase, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTCast(Type::Int, new ASTBinary(a, "==", b));
            return 0;
        });
    }

    static PStmt notEqualOp(PStmt cont) {
        return cont().accept(ECALexer::NOT_EQUAL)
                .process(exprBase, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTCast(Type::Int, new ASTBinary(a, "!=", b));
            return 0;
        });
    }

    static PStmt lessOp(PStmt cont) {
        return cont().accept(ECALexer::LESS)
                .process(exprBase, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTCast(Type::Int, new ASTBinary(a, "<", b));
            return 0;
        });
    }

    static PStmt greaterOp(PStmt cont) {
        return cont().accept(ECALexer::GREATER)
                .process(exprBase, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTCast(Type::Int, new ASTBinary(a, ">", b));
            return 0;
        });
    }

    static PStmt lessEqOp(PStmt cont) {
        return cont().accept(ECALexer::LESS_EQ)
                .process(exprBase, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTCast(Type::Int, new ASTBinary(a, "<=", b));
            return 0;
        });
    }

    static PStmt greaterEqOp(PStmt cont) {
        return cont().accept(ECALexer::GREATER_EQ)
                .process(exprBase, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTCast(Type::Int, new ASTBinary(a, ">=", b));
            return 0;
        });
    }

    static PStmt comparisonExpr(PStmt cont) {
			return cont().debug("comparisonExpr"_S).perform(exprBase).optChoose(lessOp, lessEqOp, equalOp, notEqualOp, greaterEqOp, greaterOp);
    }

    static PStmt andOp(PStmt cont) {
        return cont().accept(ECALexer::AND)
                .process(comparisonExpr, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTCast(Type::Int, new ASTBinary(
                                new ASTCast(Type::Bool, a),
                                "&&",
                                new ASTCast(Type::Bool, b)
                            ));
            return 0;
        });
    }

    static PStmt orOp(PStmt cont) {
        return cont().accept(ECALexer::OR)
                .process(comparisonExpr, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTCast(Type::Int, new ASTBinary(
                                new ASTCast(Type::Bool, a),
                                "||",
                                new ASTCast(Type::Bool, b)
                            ));
            return 0;
        });
    }

    static PStmt concatOp(PStmt cont) {
        return cont().accept(ECALexer::CONCAT)
                .process(primary, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTBinary(a, "..", b);
            return 0;
        });
    }

    static PStmt expr(PStmt cont) {
        return cont().debug("expr"_S).perform(comparisonExpr).repeatChoose(andOp, orOp);
    }


    /// STATEMENTS

    static PStmt stmtIfThenElse(PStmt cont) {
        Stmt c;
        Stmt t;
        Stmt e;
        return cont().debug("stmtIfThenElse")
					.accept(ECALexer::IF)
					.fetch(stmt, c)
					.modify([](Stmt&, Stmt& b, UserData){
							return !b->isExpression();
							}, c)
            .accept(ECALexer::THEN)
            .fetch(stmt, t)
            .accept(ECALexer::ELSE)
            .fetch(stmt, e)
            .accept(ECALexer::END)
            .modify([](Stmt& a, Stmt& c, Stmt& t, Stmt& e, UserData){
                a = new ASTConditional(new ASTCast(Type::Bool, c), t, e);
                return 0;
            }, c, t, e);
    }

    static PStmt stmtSkip(PStmt cont) {
        return cont().debug("stmtSkip").accept(ECALexer::SKIP).store(new ASTSkip());
    }

    static PStmt stmtRepeat(PStmt cont) {
        Stmt i;
        return cont().debug("stmtRepeat")
					.accept(ECALexer::REPEAT).fetch(stmt, i)
                .accept(ECALexer::BEGIN).perform(stmt)
                .accept(ECALexer::END).modify([](Stmt& a, Stmt&& i, UserData) {
            if (!i->isExpression())
                return 3;
            a = new ASTRepeat(i, a);
            return 0;
        }, std::move(i));
    }

    static PStmt stmtWhile(PStmt cont) {
        Stmt c;
        return cont().debug("stmtWhile")
					.accept(ECALexer::WHILE).fetch(stmt, c)
                .accept(ECALexer::BEGIN).perform(stmt)
                .accept(ECALexer::END).modify([](Stmt& a, Stmt&& c, UserData) {
            if (!c->isExpression())
                return 4;
            a = new ASTWhile(new ASTCast(Type::Bool, c), a);
            return 0;
        }, std::move(c));
    }

    static PStmt stmtOptions(PStmt cont) {
        return cont().debug("stmtOptions").choose(expr, stmtIfThenElse, stmtRepeat, stmtWhile, stmtSkip);
    }

    static PStmt stmtTail(PStmt cont) {
        return cont().accept(ECALexer::SEMICOLON, ECALexer::COMMA).process(stmtOptions, [](Stmt& a, Stmt&& b, UserData) {
            a = new ASTConcat(a, b);
            return 0;
        });
    }

    static PStmt stmt(PStmt cont) {
        return cont().perform(stmtOptions).repeat(stmtTail);
    }

    /// TOP LEVEL FUNCTION DEFINITIONS

    static PFunDefs defTail(PFunDefs cont) {
        return cont().process(stmt, [](FuncDefs &a, Stmt&& b, UserData) {
            a->main = b;
            return 0;
        });
    }

    static PFunDefs definition(PFunDefs cont) {
        String functionName;
        String argumentName;
        return cont()
            .accept(ECALexer::FUNCTION)
            .fetch(name, functionName)
            .accept(ECALexer::OPEN)
            .fetch(name, argumentName)
            .accept(ECALexer::CLOSE)
            .process(stmt, [](FuncDefs &a, const String& functionName, const String& argumentName, Stmt&& body, UserData) {
                if (!body->isExpression())
                    return -1;
                a->functions[functionName] = new ASTFunctionDefinition(functionName, argumentName, new ASTReturn(body));
                return 0;
            }, functionName, argumentName)
            .accept(ECALexer::END);
    }

    static PFunDefs def(PFunDefs cont) {
        return cont().store(new ASTProgram()).repeat(definition).perform(defTail).modify([](FuncDefs& a, UserData) {
        /*
            for (auto& fd : a->functions) {
                fd.second->print(std::cout);
                std::cout << std::endl;
            }

            std::cout << "main statements:" << std::endl;
            a->main->print(std::cout);
            std::cout << std::endl;
            */
            return 0;
        });
    }
};

ASTParseResult ParseECAMemory(const String &str, MemoryPool& mp) {
    ECALexer lexer(str, mp);
    auto p = ParserState<ECALexer, ECA_LOOKAHEAD>(lexer);
    ECALanguage<ECALexer>::FuncDefs result;
    ParserError retval = Parser<ECALexer, ECA_LOOKAHEAD, ECALanguage<ECALexer>::FuncDefs, MemoryPool &>(&p, mp).perform(ECALanguage<ECALexer>::def).end().retreive(result);
    if (retval) {
        TokenBase *token = p.getToken();
        std::cout << "error: " << retval.error() << std::endl;
        std::cout << "token: " << token << std::endl;
        std::cout << "token representing: " << lexer.getInputString(token) << std::endl;
        std::cout << "remaining: " << lexer.remaining() << std::endl;
        return {token ? token->start : 0, lexer.getInputString(token)};
    }
    // std::cout << "result of parse is " << result << std::endl;
    return {std::move(result)};
}

ASTParseResult ParseECAFile(const StringT& filename, MemoryPool& mp) {
    struct stat st;
    if (stat(filename.c_str(), &st)) {
        std::cerr << "error: " << strerror(errno) << std::endl;
        return {0, "File not found (stat() error)"};
    }
    int fd = open(filename.c_str(), O_RDONLY
              #ifdef O_BINARY
                  | O_BINARY
              #endif
                  );
    if (!fd) {
        std::cerr << "error: " << strerror(errno) << std::endl;
        return {0, "Could not open file (open() error)"};
    }
    char *buffer = mp.allocBytes<char>(st.st_size);
    int size = read(fd, buffer, st.st_size);
    if (size != st.st_size) {
        std::cerr << "wrong number of bytes read: " << size << " instead of "
            << st.st_size << std::endl;
        abort();
    }
    close(fd);

    String current(buffer, size);

    auto result = ParseECAMemory(current, mp);
    if (!result) {
        std::cerr << "error: " << result.error() << " at " << result.position() << std::endl;
    }
    return result;
}

#include "RuleFuncDef.h"
Rule* ASTProgram::getTypeRule() const
{
    Rule* retval = main->getTypeRule();
    for (const auto& fdentry : functions) {
        const ASTFunctionDefinition::Ref& fd = fdentry.second;
        retval = new RuleFuncDef(fd->functionName.stl(), fd->argumentName.stl(), fd->body->getTypeRule(), retval, fd);
    }
    return retval;
}

#include "RuleReturn.h"
Rule* ASTReturn::getTypeRule() const
{
    return new RuleReturn(expr->getTypeRule(), this);
}

#include "RuleSkip.h"
Rule* ASTSkip::getTypeRule() const {
    return new RuleSkip(this);
}

#include "RuleConst.h"
Rule* ASTConstant::getTypeRule() const
{
    return new RuleConst(value, this);
}

#include "RuleVar.h"
Rule* ASTVariable::getTypeRule() const
{
    return new RuleVar(name.stl(), this);
}

#include "RuleCallF.h"
Rule* ASTFunctionCall::getTypeRule() const
{
    return new RuleCallF(functionName.stl(), argument->getTypeRule(), this);
}

#include "RuleCallCmpF.h"
Rule* ASTComponentFunctionCall::getTypeRule() const
{
    return new RuleCallCmpF(componentName.stl(), functionName.stl(), argument->getTypeRule(), this);
}

#include "RuleBinOp.h"
Rule* ASTBinary::getTypeRule() const
{
    return new RuleBinOp(op.stl(), lhs->getTypeRule(), rhs->getTypeRule(), this);
}

#include "RuleAssign.h"
#include "RuleLocalDecl.h"
Rule* ASTAssignment::getTypeRule() const
{
    switch(sort){
    case AssignmentSort::LocalDecl : return new RuleLocalDecl(name.stl(), rhs->getTypeRule(), this);
    case AssignmentSort::Assign    : return new RuleAssign(name.stl(), rhs->getTypeRule(), this);
    case AssignmentSort::Unknown   : throw runtime_error("getTypeRule called on assignment with unknown sort, should determine the sort of assignment first");
    }
}

#include "RuleIf.h"
Rule* ASTConditional::getTypeRule() const
{
    return new RuleIf(c->getTypeRule(), t->getTypeRule(), e->getTypeRule(), this);
}

#include "RuleRepeat.h"
Rule* ASTRepeat::getTypeRule() const
{
    return new RuleRepeat(i->getTypeRule(), b->getTypeRule(), this);
}

#include "RuleWhile.h"
Rule* ASTWhile::getTypeRule() const
{
    return new RuleWhile(c->getTypeRule(), b->getTypeRule(), this);
}

#include "RuleExprConcat.h"
#include "RuleStmtConcat.h"
Rule* ASTConcat::getTypeRule() const
{
    if (isExpression()) {
        return new RuleExprConcat(a->getTypeRule(), b->getTypeRule(), this);
    }
    return new RuleStmtConcat(a->getTypeRule(), b->getTypeRule(), this);
}

#include "RuleFuncDef.h"
Rule* ASTFunctionDefinition::getTypeRule() const
{
    return new RuleFuncDef(functionName.stl(), argumentName.stl(), body->getTypeRule(), new RuleSkip(new ASTSkip()), this);
}

#include "RuleCast.h"
Rule* ASTCast::getTypeRule() const
{
    return new RuleCast(to, argument->getTypeRule(), this);
}
