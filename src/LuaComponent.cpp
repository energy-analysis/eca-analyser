#include "LuaComponent.h"
#include "simplestring.h"
#include "ValueInt.h"
#include "ValueString.h"

using namespace bitpowder::lib;

LuaComponent::LuaComponent(string compName, string lua, Mode mode)
{
    this->compName = compName;
    model = luaL_newstate();
    luaL_openlibs(model);
    int err;
    if(mode == file)
        err = luaL_dofile(model, lua.c_str());
    else //mode == code
        err = luaL_dostring(model, lua.c_str());
    if(err)
        throw runtime_error("Error while initializing lua model for component" + compName + ":\n\t" + string(lua_tostring(model, -1)));
}

// call lua model function with argument, handling the result with the callBack function
template<typename returnType>
returnType callLuaFunction(lua_State* model, string fName, Value* arg, returnType (*callback)(lua_State* model, string fName))
{
    /// Push function and argument on stack
    lua_getglobal(model, fName.c_str());
    ValueInt* vint = dynamic_cast<ValueInt*>(arg);
    if(vint){
        lua_Integer luaArg = static_cast<lua_Integer>(vint->getValue());
        lua_pushinteger(model, luaArg);
    } else { //Value is string
        ValueString* vstring = dynamic_cast<ValueString*>(arg);
        lua_pushstring(model, vstring->getValue().c_str());
    }
    lua_call(model, 1, 1);
    return callback(model, fName);
}

//Pop the resulting value from the stack
Value* popValue(lua_State* model, string fName)
{
    if(lua_isinteger(model, -1)){
        int i = static_cast<int>(lua_tointeger(model, -1));
        lua_pop(model, 1);
        return new ValueInt(i);
    }
    else if(lua_isstring(model, -1)){
        const char* s = lua_tostring(model, -1);
        lua_pop(model, 1);
        return new ValueString(string(s));
    } else
        throw runtime_error(fName + " does not return an integer or string");
}

//Pop the resulting integer from the stack
lua_Integer popInt(lua_State* model, string fName)
{
    if(lua_isinteger(model, -1)){
        lua_Integer i = lua_tointeger(model, -1);
        lua_pop(model, 1);
        return i;
    } else
        throw runtime_error(fName + " does not return an integer");
}



EvaluationResult LuaComponent::evaluateFunction(string funcName, Value* arg)
{
    try{
        Time time = static_cast<Time>(callLuaFunction(model, funcName + "_time", arg, popInt));
        ResourceAmount resource = static_cast<ResourceAmount>(callLuaFunction(model, funcName + "_resource", arg, popInt));
        Value* retval = callLuaFunction(model, funcName + "_definition", arg, popValue);
        return ValueResult(retval, time, resource);
    } catch(exception e) {
        throw runtime_error("Lua exception while evaluating " + compName + "." + funcName + ":\n" + e.what());
    }
}

DynResCons LuaComponent::dynResCons()
{
    lua_getglobal(model, "dyn_res_cons");
    lua_call(model, 0, 1);
    return static_cast<DynResCons>(popInt(model, "dyn_res_cons"));
}

// Based on https://stackoverflow.com/questions/1438842/iterating-through-a-lua-table-from-c
unordered_set<string> LuaComponent::getComponentFunctionNames(){
    unordered_set<string> funcNames;
    lua_pushglobaltable(model);
    lua_pushnil(model);
    while(lua_next(model, -2)) {
        if(lua_isfunction(model, -1)) {
            String s = String(lua_tostring(model, -2));
            if(s.endsWith("_definition") || s.endsWith("_time") || s.endsWith("_resource")){
                String compFuncName;
                s.rsplit("_", compFuncName);
                funcNames.insert(compFuncName.stl());
            }
        }
        lua_pop(model, 1);
    }
    lua_pop(model, 1);
    return funcNames;
}

LuaComponent::~LuaComponent()
{
    lua_close(model);
}
