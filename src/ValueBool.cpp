#include "ValueBool.h"

ValueBool::ValueBool(bool value)
{
	this->value = value;
}

bool ValueBool::getValue() const
{
	return value;
}

ValueBool::~ValueBool(){}
