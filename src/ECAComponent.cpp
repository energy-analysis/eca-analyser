#include "ECAComponent.h"
#include "FunctionEnvironment.h"

ECAComponent::ECAComponent(string name, State cState, map<string, ECAComponentFunction*> functions, Rule* dyn_res_cons)
    : cState(move(cState)), functions(move(functions))
{
    this->dyn_res_cons = dyn_res_cons;
    this->compName = name;
}

EvaluationResult ECAComponent::evaluateFunction(string funcName, Value* arg)
{
    auto res = functions.find(funcName);
    if(res == functions.end())
        throw runtime_error("Error: component " + compName + " does not have function " + compName + "." + funcName);
    ECAComponentFunction* f = res->second;
    cState.enterLocalScope();
    cState.declareLocalVar(f->getArgName(), arg);
    auto env = FunctionEnvironment();
    auto components = map<string, Component*>();
    EvaluationResult e = f->evaluateFunction(cState, env, components);
    cState.exitLocalScope();
    return e;
}

DynResCons ECAComponent::dynResCons()
{
    auto env = FunctionEnvironment();
    auto components = map<string, Component*>();
    EvaluationResult s = dyn_res_cons->evaluateRule(cState, env, components);
    ValueInt* vInt = dynamic_cast<ValueInt*>(s.getValue());
    if(vInt) {
        int x = vInt->getValue();
        return static_cast<DynResCons>(x);
    } else {
        throw runtime_error("phi function of component \"" + compName + "\" produced non-integer value");
    }
}

unordered_set<string> ECAComponent::getComponentFunctionNames(){
    unordered_set<string> funcNames;
    for(auto entry : functions){
        funcNames.insert(entry.first);
    }
    return funcNames;
}
