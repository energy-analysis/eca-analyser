#include "RuleConst.h"
#include "FunctionEnvironment.h"

RuleConst::RuleConst(Value* value, const AST* ast)
{
    this->constValue = value;
    this->ast = ast;
}

EvaluationResult RuleConst::evaluateRule(State&, FunctionEnvironment& env, map<string, Component*>& components)
{
    Time time = env.getTConst();
    ResourceAmount resource = td_ec(components, time);
    return ValueResult(constValue, time, resource);
}

vector<Rule*>* RuleConst::getChildren()
{
    return new vector<Rule*>();
}

RuleConst::~RuleConst()
{
    //dtor
}
